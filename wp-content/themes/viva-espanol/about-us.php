<?php /* Template Name: View About Us */

get_header();

rewind_posts();

$page = array('page_id' => 20,
'post_type' => 'page',
'post_status' => 'publish' );

$my_query = new WP_Query($page);

while($my_query->have_posts()) : $my_query->the_post();

// Get desktop image banner
$imageSection = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
$imageSection = $imageSection[0];

$titleSection = get_post_meta( $post->ID, 'title_page', true);

?>



<section class="main-section-nopadding about-ves" id="about-us">
    <div class="container-fluid">
        <div class="row">
            <div class="container-max-dk">
                <div class="about-ves__content">
                    <div class="about-ves__inner">
                        <?= $titleSection; ?>
                        <div class="about-ves__content-image">
                            <img class="desktop-image" src="<?= $imageSection;  ?>" alt="">
                        </div>
                        <?= the_content();  ?>
                        <!--ul class="about-ves__list">
                            <li class="about-ves__list--item">
                                <svg viewBox="0 0 305.9 311.5" class="bg-rose">
                                    <path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path>
                                </svg>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore accusantium repudiandae assumenda dolorem quaerat esse itaque, quia repellendus delectus quo id vel omnis quis, culpa hic, pariatur aperiam voluptates veniam?
                            </li>
                            <li class="about-ves__list--item">
                            <svg viewBox="0 0 305.9 311.5" class="bg-blue">
                                    <path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path>
                                </svg>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore accusantium repudiandae assumenda dolorem quaerat esse itaque, quia repellendus delectus quo id vel omnis quis, culpa hic, pariatur aperiam voluptates veniam?
                            </li>
                            <li class="about-ves__list--item">
                            <svg viewBox="0 0 305.9 311.5" class="bg-orange">
                                    <path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path>
                                </svg>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore accusantium repudiandae assumenda dolorem quaerat esse itaque, quia repellendus delectus quo id vel omnis quis, culpa hic, pariatur aperiam voluptates veniam?
                            </li>
                            <li class="about-ves__list--item">
                            <svg viewBox="0 0 305.9 311.5" class="bg-green">
                                    <path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path>
                                </svg>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore accusantium repudiandae assumenda dolorem quaerat esse itaque, quia repellendus delectus quo id vel omnis quis, culpa hic, pariatur aperiam voluptates veniam?
                            </li>
                            <li class="about-ves__list--item">
                            <svg viewBox="0 0 305.9 311.5" class="bg-purple">
                                    <path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path>
                                </svg>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore accusantium repudiandae assumenda dolorem quaerat esse itaque, quia repellendus delectus quo id vel omnis quis, culpa hic, pariatur aperiam voluptates veniam?
                            </li>
                            <li class="about-ves__list--item">
                            <svg viewBox="0 0 305.9 311.5" class="bg-yellow">
                                    <path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path>
                                </svg>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore accusantium repudiandae assumenda dolorem quaerat esse itaque, quia repellendus delectus quo id vel omnis quis, culpa hic, pariatur aperiam voluptates veniam?
                            </li>
                            <li class="about-ves__list--item">
                            <svg viewBox="0 0 305.9 311.5" class="bg-rose">
                                    <path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path>
                                </svg>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore accusantium repudiandae assumenda dolorem quaerat esse itaque, quia repellendus delectus quo id vel omnis quis, culpa hic, pariatur aperiam voluptates veniam?
                            </li>
                        </ul-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>

<section class="main-section our-programs-ves" id="our-programs">
    <div class="container-fluid">
        <div class="row">
            <div class="container-max-dk">
                <div class="our-programs-ves__title">
                    <h2 class="text-left color-green">Our Programs</h2>
                </div> 
                <div class="our-programs-ves__content">
                    
                    <?php
                    
                        rewind_posts();

                        // Get all items in slider post type
                        $query = new WP_Query( array(
                            'post_type' => 'Our Programs',
                            'posts_per_page' => -1 
                        ) );
                                
                    ?>

                    <?php 
                        $index = 0;
                        while($query->have_posts()) : $query->the_post(); 

                        // Get desktop image banner
                        $imageProgram = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
                        $imageProgram = $imageProgram[0];

                        // Get subtitle and color
                        $homeBannerSubtitle = get_post_meta( $post->ID, 'subtitle_slide', true);
                        $homeBannerSubtitleColor = rwmb_meta( 'subtitle_slide_color' );

                        $program = rwmb_meta( 'radio_programs' );

                        

                    ?>

                        <div class="our-programs-ves__program" index="<?= $index; ?>">
                            <div class="our-programs-ves__program--image <?= $program; ?>"> 
                                <img src="<?= $imageProgram; ?>" alt="">
                            </div>
                            <div class="our-programs-ves__program--text">
                                <h3 class="text-center color-green"><?= the_title(); ?></h3>
                            </div>
                        </div>

                    <?php $index++; endwhile; ?>

                    <!--div class="our-programs-ves__program">
                        <div class="our-programs-ves__program--image ">
                            <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/teacher-main.jpg" alt="">
                        </div>
                        <div class="our-programs-ves__program--text">
                            <h3 class="text-center color-green">Pre-K Kindergarden</h3>
                        </div>
                    </div>
                    <div class="our-programs-ves__program">
                        <div class="our-programs-ves__program--image bg-blue">
                            <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/teacher-main.jpg" alt="">
                        </div>
                        <div class="our-programs-ves__program--text">
                            <h3 class="text-center color-green">Pre-K Kindergarden</h3>
                        </div>
                    </div>
                    <div class="our-programs-ves__program">
                        <div class="our-programs-ves__program--image bg-rose-dark">
                            <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/teacher-main.jpg" alt="">
                        </div>
                        <div class="our-programs-ves__program--text">
                            <h3 class="text-center color-green">Pre-K Kindergarden</h3>
                        </div>
                    </div>
                    <div class="our-programs-ves__program">
                        <div class="our-programs-ves__program--image bg-orange">
                            <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/teacher-main.jpg" alt="">
                        </div>
                        <div class="our-programs-ves__program--text">
                            <h3 class="text-center color-green">Pre-K Kindergarden</h3>
                        </div>
                    </div>
                    <div class="our-programs-ves__program">
                        <div class="our-programs-ves__program--image bg-purple">
                            <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/teacher-main.jpg" alt="">
                        </div>
                        <div class="our-programs-ves__program--text">
                            <h3 class="text-center color-green">Pre-K Kindergarden</h3>
                        </div>
                    </div>
                    <div class="our-programs-ves__program">
                        <div class="our-programs-ves__program--image bg-green">
                            <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/teacher-main.jpg" alt="">
                        </div>
                        <div class="our-programs-ves__program--text">
                            <h3 class="text-center color-green">Pre-K Kindergarden</h3>
                        </div>
                    </div-->
                </div>
                <div class="our-programs-ves__carousel">
                    <?php 
                        
                        while($query->have_posts()) : $query->the_post(); 

                        $location = rwmb_meta('location');
                        $program = rwmb_meta( 'radio_programs' );
                        $description = rwmb_meta( 'radio_programs' );

                        $images_download = rwmb_meta( 'download_image' );
                        $text_download = rwmb_meta( 'download_text' );
                        $documents_download = rwmb_meta( 'download_document' );
                        $block = rwmb_meta( 'download_check' );
                        $download_link = rwmb_meta( 'download_link' );
                        

                    ?>
                    <div class="our-programs-ves__carousel--item <?= $program; ?>">
                        <div class="our-programs-ves__carousel--title">
                            <h3><?= the_title(); ?></h3>
                        </div>
                        <div class="our-programs-ves__carousel--location">
                            <?php if($location){ ?>
                            <p class="color-gray">
                                <b class="color-black">Location:</b>
                                <?= $location; ?>
                            </p>
                            <?php } ?>
                            <p class="color-gray"><?= the_content(); ?></p>
                        </div>
                        <div class="our-programs-ves__carousel--download">
                            <div class="our-programs-ves__carousel--download--col">
                                <div class="our-programs-ves__carousel--download--image">
                                    <?php foreach ( $images_download as $image_download ) { ?>
                                        <img src="<?php echo $image_download['url']; ?>" alt="">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="our-programs-ves__carousel--download--col">
                                <div class="our-programs-ves__carousel--download--text">
                                    <p class="color-gray"><?= $text_download; ?></p>
                                    <!-- <p class="color-gray"><?= $text_download; ?></p>
                                    <?php foreach ( $documents_download as $document_download ) { ?>
                                        <a href="<?php echo $document_download['url']; ?>" class="btn centered bordered full bg-green background-hover-green--line">Download</a>
                                    <?php } ?>-->

                                    <?php if(!empty($download_link)){ ?>
                                    <p class="color-gray"><?= $download_link; ?></p>
                                    <?php }else{?>
                                    <a <?php if($block){ echo 'style="display:none;"';} ?> class="more-info" href="https://campscui.active.com/orgs/LamorindaSpanishIncdbaVivaelEspanol#/selectSessions/2873804" target="_blank" target="_blank">Find out more information and register <br/> <span>click here</span></a>
                                        <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <!--div class="our-programs-ves__carousel--item">
                        <div class="our-programs-ves__carousel--title">
                            <h3>Pre-School-Kindergarden Classes</h3>
                        </div>
                        <div class="our-programs-ves__carousel--location">
                            <p class="color-gray">
                                <b class="color-black">Location:</b>
                                Lafayette, San Rafael, Pleasaton
                            </p>
                            <p class="color-gray">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste velit ea autem quaerat tempora quo assumenda, consequatur rem quasi incidunt, laboriosam, nesciunt eveniet excepturi! Voluptatem vitae quam dolores ea quo.</p>
                        </div>
                        <div class="our-programs-ves__carousel--download">
                            <div class="our-programs-ves__carousel--download--col">
                                <div class="our-programs-ves__carousel--download--image">
                                    <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/bg-inperson.jpg" alt="">
                                </div>
                            </div>
                            <div class="our-programs-ves__carousel--download--col">
                                <div class="our-programs-ves__carousel--download--text">
                                    <p class="color-gray">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nihil explicabo error labore atque! Magnam ex, et expedita similique veniam doloremque officiis recusandae assumenda! Sit voluptatem error a quidem eius ipsum?</p>
                                    <a href="" class="btn centered bordered full bg-green background-hover-green--line">Download</a>
                                </div>
                            </div>
                            
                        </div>
                    </div-->
                </div>
                <!-- <div class="our-programs-ves__cta">
                    <a href="">Find out more information and register <br/> <span>click here</span></a>
                </div> -->
            </div>   
        </div>
    </div>
</section>

<!--section class="main-section-nopadding table-ves-cta" id="online-table">
    <div class="container-fluid">
        <div class="row">
            <div class="table-ves-cta__content">
                <a class="btn bordered centered background-purple background-hover-purple-line color-white large" href="https://campscui.active.com/orgs/LamorindaSpanishIncdbaVivaelEspanol#/selectSessions/2873804" target="_blank">Register here</a>
            </div> 
        </div>
    </div>
</section-->

<section id="gallery" class="main-section-nopadding gallery-ves">
			<div class="container-fluid">
				<div class="row">
					<div class="gallery-ves__content">
					<div class="gallery-ves__title">
						<h2 class="text-center color-blue">Gallery</h2>
					</div>
					<div class="gallery-ves__wrapper">
						<div class="gallery-ves__carousel">
							<?php 

								$query = new WP_Query(array(
									'post_type' => 'gallery',
									'posts_per_page' => -1
								));

								while ($query->have_posts()) : $query->the_post();

								$gallery = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
								$gallery = $gallery[0];

							?>
							<div class="gallery-ves__carousel--item">
								<img src="<?= $gallery; ?>" alt="">
							</div>
							<?php endwhile; ?>
		
						</div>
					</div>
					</div>			
				</div>
			</div>
	</section>

<?php
get_footer();
