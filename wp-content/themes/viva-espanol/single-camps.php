<?php

get_header();

global $post;

rewind_posts();

$page = array(
    'page_id' => 200,
    'post_type' => 'page',
    'post_status' => 'publish'
);

$my_query = new WP_Query($page);
$title_section = get_the_title();

while ($my_query->have_posts()) : $my_query->the_post();

?>

<section class="main-section-nopadding person-ves" id="in-person">
    <div class="container-fluid">
        <div class="row">
            <div class="person-ves__intro">
                <h1 class="color-white">In-Person <?= $title_section; ?></h1>
                <p class="color-white"><?= get_the_excerpt(); ?></p>
            </div>
            <!--div class="person-ves__cta">
                <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
            </div-->
            <div class="person-ves__content">
                <div class="person-ves__selection">
                    <!--p class="text-center color-purple"><strong>Please select your location</strong></p-->
                    <?php include_once('template-parts/in-person-selection.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>


    <?php 

endwhile;

    rewind_posts();

    if (have_posts()) : while (have_posts()) : the_post();

    ?>
    <section class="main-section camps-ves">
        <div class="container-fluid">
            <div class="row">
                <div class="container-max-dk">
                    <div class="camps-ves__container">
                        <?php 
                            the_content();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php endwhile;
endif; ?>

<?php
get_footer();
