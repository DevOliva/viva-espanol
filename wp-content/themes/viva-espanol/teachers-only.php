<?php /* Template Name: Teachers Only */

get_header();

$page = array(
    'page_id' => 172,
    'post_type' => 'page',
    'post_status' => 'publish'
);

$my_query = new WP_Query($page);

while ($my_query->have_posts()) : $my_query->the_post();

?>


<section class="main-section-nopadding teachers-only">
        <div class="container-fluid">
            <div class="row">
                <div class="container-max-dk">
                <div class="teachers-only__content">
                    <div class="teachers-only__main">
                        <div class="teachers-only__main--title">
                            <h2 class="color-rose-dark"><?= the_title(); ?></h2>
                            <p class="color-gray"><?= get_the_excerpt(); ?></p>
                        </div>
                        <div class="teachers-only__main--content">
                            <?= the_content();  ?>

                        </div>
                        <div class="teachers-only__main--drives">
                            <?php 
                            
                                $drives = get_post_meta( $post->ID, 'drives_group', true); 

                                foreach( $drives as $drive){
                                
                                    $title = isset( $drive['button_name'] ) ? $drive['button_name'] : array();
                                    $url = isset( $drive['button_url'] ) ? $drive['button_url'] : array();
                                
                            ?>
                            <div class="teachers-only__main--drives--item">
                                <h3><?= $title; ?></h3>
                                <a href="<?= $url; ?>" target="_blank" class="btn bordered centered">Entrar</a>
                            </div>
                            <?php } ?>
                            <!-- <div class="teachers-only__main--drives--item">
                                <h3>Enseñanza Virtual</h3>
                                <a href="" class="btn bordered centered bg-green background-hover-green--line">Enter</a>
                            </div>
                            <div class="teachers-only__main--drives--item">
                                <h3>Enseñanza Virtual</h3>
                                <a href="" class="btn bordered centered bg-green background-hover-green--line">Enter</a>
                            </div> -->
                        </div>
                    </div>
                </div>
                </div>             
            </div>
        </div>
</section>

<?php endwhile; ?>

<? 
get_footer();