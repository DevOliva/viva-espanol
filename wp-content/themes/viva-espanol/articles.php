<?php /* Template Name: Articles */ 

get_header();

rewind_posts();

$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
$image = $image[0];

?>

<section class="main-section-nopadding articles-ves">
    <div class="container-fluid">
        <div class="row">
            <div class="articles-ves__title">
                <h1><?php the_title(); ?></h1>
                <img src="<?= $image; ?>" alt="">
            </div>
            <div class="articles-ves__content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<?php 
    get_footer();