//import Header from './Header.js';
// import Translation from './Translation.min.js';
// import Home from './Home.min.js';
// import Formulario from './Formulario.min.js';

const APP = window.APP || {};


const initApp = () => {
  window.APP = APP

  const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  APP.isMobile = isMobile

  // APP.Translation = new Translation();

  // APP.Loader = new Loader();
  //
  // APP.Loader.on('complete', () =>{
  //     console.log('site loaded');
  // });

  var Header = function() {
    console.log(window.location.pathname);
    var url = window.location.pathname;
    if(url.includes('online')){
      //$('.site-menu__item').eq(3).children('a').addClass('current');
      $('.site-menu__item[data-name="online').children('a').addClass('current');
    }else if(url.includes('classes') || url.includes('camps')){
      $('.site-menu__item[data-name="in-person').children('a').addClass('current');
      //$('.site-menu__item').eq(2).children('a').addClass('current');
    }
    const nav_icon = document.querySelector('.nav-toggle').addEventListener('click', function(e){

      if($(this).hasClass('open')){
        $(this).removeClass('open');
        $('.site-menu').stop().animate({
          "right": "-260px"
        }, "slow");
      }else{
        $(this).addClass('open');
        $('.site-menu').stop().animate({
          "right": "0"
        }, "slow");
      }
    });

  };

  var Slider = function() {
    
    var slideHome = $('.ves-slider').slick({
      //dots: true,
      infinite: true,
      //speed: 500,
      //autoplay: true,
      //autoplaySpeed: 5000,
      //fade: true,
      arrows: true,
      //cssEase: 'linear'
      prevArrow:"<button class='slick-prev slick-arrow' aria-label='Previous' type='button' style=''><span></span></button>",
      nextArrow:"<button class='slick-next slick-arrow' aria-label='Next' type='button' style=''><span></span></button>"
    });

    slideHome.on("beforeChange", function(event, slick) {
      player.mute();
    });
    slideHome.on("afterChange", function(event, slick, currentSlide, nextSlide) {
      if(currentSlide == 0){
        player.unMute();
      }
    });

    var video = document.getElementById("video-ves") ;   

    $('.ves-item button.video-control').on("click", function(e){

      $(this).toggleClass('unmuted');
      if (player.isMuted()){
        player.unMute();
      }else{
        player.mute();
      }
      
    });

  };

  var Teachers = function() {

    $('.teachers-ves__carousel').slick({
      dots: true,
      arrows: true, 
      infinite: true,
      slidesToShow: 6,
      slidesToScroll: 6,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 5000,
      //fade: true,
      arrows: false,
      //cssEase: 'linear'
      responsive: [
        {
          breakpoint: 1680,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
          }
        },
        {
          breakpoint: 1440,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
          }
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          }
        },
        {
          breakpoint: 360,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });

  };

  var Gallery = function(){

    $('.gallery-ves__carousel').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: true,
    });

  };

  var Programs = function(){

    $('.our-programs-ves__carousel').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
      //autoplay: true,
      autoplaySpeed: 5000,
      arrows: true,
    });

    $('.our-programs-ves__program--image img, .our-programs-ves__program--text h3').on('click', function(e) {
      e.preventDefault();
      var slideTo = $(this).parents('.our-programs-ves__program').attr('index');
      console.log(slideTo);
      $('.our-programs-ves__carousel').slick('slickGoTo', slideTo);
      $('body, html').stop().animate({
        scrollTop: $('.our-programs-ves__carousel').offset().top
      });
    });

  };

  var Online = function(){

    $('.online-table-ves__body > ul li a').on('click', function(e){
      e.preventDefault();
      $(this).toggleClass('open').next().stop().slideToggle();

    });


    $('.online-table-ves__search select').on('change', function(){

      var value = $(this).children("option:selected").val();
      $('.online-table-ves__desktop .pdt, .online-table-ves__desktop .mdt, .online-table-ves__desktop .cdt, .online-table-ves__desktop .edt').hide();

      $('.online-table-ves__desktop .'+value+'').show();

    });

    $('.online-ves__types--item a').on('click', function(e){
      
      var hrefURL, pageURL;
      hrefURL = new URL($(this).attr('href'));
      pageURL = new URL(window.location);

      if (hrefURL.href === pageURL.href) {
        $('html, body').stop().animate({
               scrollTop: $('#online-table').offset().top 
             }, 1000);
        return e.preventDefault();
      }

    });
    

  };

  var InPerson = function(){

    $('.person-ves__table__mobile--body > ul li a').on('click', function(e){
      e.preventDefault();
      $(this).toggleClass('open').next().stop().slideToggle();

    });

  };

  var Select = function(){

    var x, i, j, l, ll, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("custom-select");
    l = x.length;
    for (i = 0; i < l; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    ll = selElmnt.length;
    /* For each element, create a new DIV that will act as the selected item: */
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /* For each element, create a new DIV that will contain the option list: */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < ll; j++) {
        /* For each option in the original select element,
        create a new DIV that will act as an option item: */
        // c = document.createElement("DIV");
        // c.innerHTML = selElmnt.options[j].innerHTML;

        c = document.createElement("a");
        c.setAttribute("href", selElmnt.options[j].value);
        c.innerHTML = selElmnt.options[j].innerHTML;

        c.addEventListener("click", function(e) {
            /* When an item is clicked, update the original select box,
            and the selected item: */
            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            for (i = 0; i < sl; i++) {
            if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                y = this.parentNode.getElementsByClassName("same-as-selected");
                yl = y.length;
                for (k = 0; k < yl; k++) {
                y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
            }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
        /* When the select box is clicked, close any other select boxes,
        and open/close the current select box: */
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
    }

    function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
    except the current select box: */
    var x, y, i, xl, yl, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
        arrNo.push(i)
        } else {
        y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
        }
    }
    }

    /* If the user clicks anywhere outside the select box,
    then close all select boxes: */
    document.addEventListener("click", closeAllSelect); 

  };

  var Newsletter = function() {

    var valida_back = false;
    $(document).on('keypress change', '.footer-newsletter input', function(){

      var expr = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
      valida_back = false;
      if($(this).val().length >= 3 && expr.test($(this).val())){
        $(this).removeClass('error');
        $(this).addClass('validado');
        valida_back = true;
      }else{
        $(this).removeClass('validado');
        $(this).addClass('error');
        valida_back = false;
    }

    });

    $(document).on('click', '.footer-newsletter form button', function(e){
      e.preventDefault();
      $('.form-message').text('');
      if(!valida_back){ 
        $('.form-message').text('Write a valid email.')
        return false; 
      } else {

        var data = $('.footer-newsletter form').serialize();
        $.ajax({
          type: "POST",
          data: data,
          url: 'http://localhost:8080/Free/viva-espanol/wp-content/themes/viva-espanol/newsletter.php',
          beforeSend: function() {
            // setting a timeout
            $('.form-message').text('Sending...');
            $('.footer-newsletter form button').hide();
          },
          success: function(data){

            $('.form-message').text(data);

            setTimeout( function(){
              $('.footer-newsletter form')[0].reset();
              $('.form-message').text('');
              $('.footer-newsletter form button').show();
            }, 10000);
            
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
          }
        });

      }

    });
    
  };

  var Menu = function(){

    $('.site-menu__link').on('click', function(e){
      console.log('clicked');
      if(APP.isMobile){
        console.log('mobile');
        if($(this).attr('href') == '#'){
          console.log('href');
          if($(this).siblings('.site-menu__dropdown').length){
            e.preventDefault();
            $(this).toggleClass('open');
            $(this).siblings('.site-menu__dropdown').slideToggle();
          }
        }
        
      }

    });

    // $('.gallery-scroll').on('click', function(){
    //   var hash = $(this).attr('href');
    //   document.location.href="/"+hash;
    // });

  };

  var Modal = function(){

    $('.modal-open').on('click', function(e){
      e.preventDefault();

      var modal = $(this).attr('data-modal');

      $('html, body').addClass('modal-open');

      $(modal).fadeIn();

    });

    $('.modal-close, .modal-bg').on('click', function(){
      $('.modal').fadeOut();
      $('html, body').removeClass('modal-open');
    });

  };

  var Footer = function(){
      $(window).on('scroll', function(){
        var scroll = $(this).scrollTop();

        if(scroll > 600){
          $('.backto-top').fadeIn();
        }else{
          $('.backto-top').fadeOut();
        }
      });

      $('.backto-top').on('click', function(){
        $('html, body').stop().animate({
          scrollTop: 0
        }, 1000);
      });
  };


  APP.Header = new Header();
  APP.Slider = new Slider();
  APP.Teachers = new Teachers();
  APP.Gallery = new Gallery();
  APP.Gallery = new Programs();
  APP.Programs = new Online();
  APP.InPerson = new InPerson();
  APP.Select = new Select();
  APP.Newsletter = new Newsletter();
  APP.Menu = new Menu();
  APP.Modal = new Modal();
  APP.Footer = new Footer();
  //APP.Formulario = new Formulario();


};

if(document.readyState === 'complete' || (document.readyState !== 'loading' &&
!document.documentElement.doScroll)){
  initApp();
} else {
  document.addEventListener('DOMContentLoaded', initApp);
}
