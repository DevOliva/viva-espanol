'use strict';

var   fs      = require('fs'),
url           = require('url'),
glob          = require('glob'),
gulp          = require('gulp'),
sass          = require('gulp-sass'),
uglify        = require('gulp-uglify-es').default,
concat        = require('gulp-concat'),
gutil         = require('gulp-util'),
fileinclude   = require('gulp-file-include'),
browserSync   = require('browser-sync').create(),
http          = require('http'),
cleanCSS      = require('gulp-clean-css'),
rename        = require('gulp-rename'),
htmlmin       = require('gulp-htmlmin'),
imagemin      = require('gulp-imagemin'),
path          = require('path'),
inject        = require('gulp-inject'),
staticI18nHtml = require('gulp-static-i18n-html'),
data          = require('gulp-data'),
sourcemaps = require('gulp-sourcemaps'),
source = require('vinyl-source-stream'),
buffer = require('vinyl-buffer'),
rollup = require('@rollup/stream'),
babel = require('@rollup/plugin-babel'),
commonjs = require('@rollup/plugin-commonjs'),
nodeResolve = require('@rollup/plugin-node-resolve'),
browserify = require('browserify'),
babelify = require('babelify'),
    connect = require("gulp-connect"),
    source = require("vinyl-source-stream");

    gulp.task('icons', function() {
        return gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
            .pipe(gulp.dest('dist/assets/webfonts/'));
    });
sass.compiler = require('node-sass');


/* Fonts */
gulp.task('copyFonts', gulp.series(async function(){
  return gulp.src(['src/fonts/**/*.{ttf,eot,svg,woff,woff2}'])
  .pipe(gulp.dest('dist/assets/fonts'));
}));

/* Images */
gulp.task('imgSquash', gulp.series(async function(){
  return gulp.src(['src/assets/images/**/*'])
  // .pipe(imagemin())
  .pipe(gulp.dest('dist/assets/images'));
}));

/* PHP */
gulp.task('phpFiles', gulp.series(async function(){
  return gulp.src(['src/*.php'])
  .pipe(gulp.dest('../'));
}));

/* HTML */
gulp.task('html-minifed', gulp.series(async function(){
  return gulp.src([
    'src/html/*.html'
  ])
  .pipe(htmlmin({
    collapseWhitespace:true,
    ignoreCustomFragments: [ /<%[\s\S]*?%>/, /<\?[=|php]?[\s\S]*?\?>/ ]
  }))
  .pipe(gulp.dest('dist'))
}));

gulp.task('sass', async function(){
  return gulp.src('src/sass/main.scss')
  .pipe(sass().on('error', sass.logError))
  .pipe(concat('style.min.css'))
  .pipe(cleanCSS({compatibility: 'ie8'}))
  .pipe(gulp.dest([
    'dist/assets/css'
  ]))
  .pipe(browserSync.stream());
});

gulp.task('vendor-css', async function(){
  return gulp.src([
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/slick-carousel/slick/slick-theme.css'
  ])
  .pipe(concat('core.min.css'))
  .pipe(cleanCSS({compatibility: 'ie8'}))
  .pipe(gulp.dest([
    'dist/assets/css'
  ]))
  .pipe(browserSync.stream());
});

gulp.task('vendor-js', async function(){
  return gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/slick-carousel/slick/slick.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/axios/dist/axios.min.js',
    'node_modules/imagesloaded/imagesloaded.pkgd.js'
  ])
  .pipe(concat('main-core.js'))
  .pipe(uglify().on("error", gutil.log))
  .pipe(rename({ extname : '.min.js'}))
  .pipe(gulp.dest([
    'dist/assets/js'
  ]))
});

gulp.task('js', async function(){
  return gulp.src([
    'src/js/*.js'
  ])
  .pipe(concat('main.js'))
  .pipe(uglify().on("error", gutil.log))
  .pipe(rename({ extname : '.min.js'}))
  .pipe(gulp.dest([
    'dist/assets/js'
  ]))
  .pipe(browserSync.stream({match: 'src/js/**/*.js'}));
});


gulp.task('fileInclude', async function(){
  gulp.src(['src/*.html'])
  .pipe(fileinclude({
    prefix: '@@'
  }))
  .pipe(gulp.dest('src/html'))
});

gulp.task('serve', async function(){

  browserSync.init({
    logPrefix: 'Viva el español',
    host: 'http://localhost:8080/Free/viva-espanol/',
    port: 3060,
    open: false,
    notify: false,
    ghost: false,
    files: [
      'dist/**/*'
    ]
  });


});

gulp.task('watch', async function(){

  gulp.watch('src/assets/fonts/**/*', gulp.series(['copyFonts']));
  gulp.watch('src/assets/images/**/*', gulp.series(['imgSquash']));

  gulp.watch('src/**/*.scss', gulp.series('sass'));
  gulp.watch('src/**/*.js', gulp.series('js'));
  gulp.watch('src/*.html', gulp.series('fileInclude'));

  gulp.watch('src/*/**.html').on('change', gulp.series(['html-minifed']));
  gulp.watch('src/**/*.js').on('change', browserSync.reload);
});



gulp.task('default', gulp.parallel([
  'copyFonts',
  'imgSquash',
  'fileInclude',
  'phpFiles',
  'vendor-js',
  'js',
  'icons',
  'vendor-css',
  'html-minifed',
  'sass',
  'serve',
  'watch',
]));
