<?php
/**
 * Front page template
 *
 * @package Viva el español
 */


get_header();

?>

	<section class="main-section">
		<div class="container-fluid">
			<div class="row">
				<div class="ves-container">
					<div class="ves-slider">
					<?php

						rewind_posts();

						// Get all items in slider post type
						$query = new WP_Query( array(
						'post_type' => 'Slider',
						'posts_per_page' => -1 ) );

						while($query->have_posts()) : $query->the_post();

						$homeBannerImageDesktop = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
						$homeBannerImageDesktop = $homeBannerImageDesktop[0];
						$homeBannerTitle = get_post_meta( $post->ID, 'title_slide', true);

						$value = rwmb_meta( 'radio_selection' );

					?>
						<div class="ves-item">
							<?php if($value == 'image'): ?>
								<img src="<?= $homeBannerImageDesktop; ?>" alt="">
							<?php elseif ($value == 'video'): 
								
								$videos = rwmb_meta( 'video_field' );

								foreach ( $videos as $video ) {
							?>

									<!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
							<div id="player"></div>

								<!-- <video id="video-ves" autoplay muted loop>
									<source src="<?php echo $video['src']; ?>" type="video/mp4">
								</video>  -->
								<img class="responsive-video" src="<?= $homeBannerImageDesktop; ?>" alt="">
								<button class="video-control unmuted"></button>
								
							<?php }
						 	endif; ?>
							<div class="ves-item__content">
								<div class="ves-item__content-centered">
									<div class="content-wrapper">
										<p class="text"><?= $homeBannerTitle;  ?></p>

										<?php

											$group_botones = rwmb_meta( 'button_group' );

											if ( ! empty( $group_botones ) ) {

												foreach ( $group_botones as $group_boton ) {
													// Name of the button
													$homeBannerButton = isset( $group_boton['button_name'] ) ? $group_boton['button_name'] : array();
													$homeBannerButtonURL = isset( $group_boton['button_url'] ) ? $group_boton['button_url'] : array();
													
													echo '<a class="btn btn-bordered" href="' . $homeBannerButtonURL . '">' . $homeBannerButton . '</a>';
												}
											}

										?>
									</div>
								</div>
							</div>
						</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</section>
		
		<section id="welcome" class="main-section-nopadding welcome-ves">
			<div class="container-fluid">
				<div class="row">
					
					<h1 class="text-center">
							<span class="color-purple">W</span><span class="color-rose">e</span><span class="color-yellow">l</span><span class="color-green">c</span><span class="color-orange">o</span><span class="color-blue">m</span><span class="color-rose-dark">e </span><span class="color-purple">t</span><span class="color-rose">o</span>
							<br/>
							<span class="color-green">¡</span><span class="color-orange">V</span><span class="color-blue">i</span><span class="color-rose-dark">v</span><span class="color-purple">a </span><span class="color-yellow">e</span><span class="color-green">l </span><span class="color-rose-dark">E</span><span class="color-purple">s</span><span class="color-green">p</span><span class="color-blue">a</span><span class="color-purple">ñ</span><span class="color-rose">o</span><span class="color-green">l</span><span class="color-yellow">!</span>
						</h1>
						<div class="welcome-ves__content">
							<div class="container-max-dk">
								<h2>¡Bienvenidos!</h2>
								<div class="welcome-ves__content-image">
									<img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/welcome-image.jpg" alt="">
								</div>
									
									<p class="subtext">Give your child the gift of <span class="">foreign language</span>!</p>
									<p class="text">We believe that learning a <span>foreign language</span> in the pre-school and elementary school years is an essential part of a child’s education and development. Today’s children need to be prepared to live and work in a global context.</p>
									<p class="subtext">Don't wait to get started - <span class="background-welcome background-purple">begin early</span></p>
									<p class="text"><span><a href="/wp-content/uploads/2021/01/Why-How-When.pdf" target="_blank">Scientific studies</a></span> indicate that there are sound physiological and psychological reasons to initiate <span>foreign language</span> study with children at an early age. <a href="/wp-content/uploads/2021/01/Why-How-When.pdf" target="_blank">Cognitive research</a> shows that very young children <span>learn language</span> easily due to the <span>elasticity of the brain</span>.</p>
									<p class="text">In fact, this research states that the <span>learning window</span>, when the peak acquisition of a foreign language occurs, is <span>between birth and ten years of age</span>. Yet, for <span>most students</span>, foreign language is introduced and formally studied when they are twelve years of age or older.</p>
									<p class="text">In addition, studies have shown —<span>and experience has supported</span>— that children who learn a language before the onset of adolescence are much more likely to have <span>native-like pronunciation</span>.</p>
									<a class="btn bordered centered background-rose-dark background-hover-rose-dark" href="<?php echo get_home_url(); ?>/about-us">Discover all about<br/>our programs!</a>
							</div>
					</div>				
				</div>
			</div>
		</section>
		<section id="expectations" class="main-section expectations-ves">
		<div class="container-fluid">
			<div class="row">
				<div class="container-max-dk">
					<div class="expectations-ves__content">
						<div class="expectations-ves__inner">
							<h2 class="text-center color-rose">What sould I expect to see in a <br/>
						
								<span class="color-green">¡</span><span class="color-orange">V</span><span class="color-blue">i</span><span class="color-rose-dark">v</span><span class="color-purple">a </span><span class="color-yellow">e</span><span class="color-green">l </span><span class="color-rose-dark">E</span><span class="color-purple">s</span><span class="color-green">p</span><span class="color-blue">a</span><span class="color-purple">ñ</span><span class="color-rose">o</span><span class="color-green">l</span><span class="color-yellow">!</span>
								
								class?
							</h2>
							<ul class="expectations-ves__list">
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-rose">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									High-quality instruction by native Spanish speakers.
								</li>
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-purple">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									98-100% use of the Spanish language.</li>
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-green">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									Incorporation of learning activities that are culturally based.</li>
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-yellow">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									Teaching around a theme or subject area.</li>
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-rose">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									High-quality manipulatives, visuals, music and cultural items from Spanish-speaking countries.</li>
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-blue">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									Activities that encourage learning through active participation, including singing, dancing, movement, role-playing, games, stories, arts and crafts, cooking and food projects.</li>
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-yellow">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									Students grouped by level of proficiency rather than strictly by grade level.</li>
								<li class="expectations-ves__list--item">
								<svg viewBox="0 0 305.9 311.5" class="bg-orange">
								<path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6
									l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z
									M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z
									M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5
									C209.3,164.1,213.1,167.8,213.1,172.4z"/>
								</svg>
									Assessment of students’ progress on a regular basis.</li>
							</ul>
							<div class="expectations-ves__fun">
								<span class="text-center color-rose">Now let's have fun!</span>
								<p class="text text-center color-green">Join us to sing, dance, cook, paint, and play games in español!</p>
								<a href="<?php echo get_home_url(); ?>/about-us" class="btn centered bg-purple background-hover-purple">Learn more about our programs</a>
							</div>
						</div>
					</div>
					<div class="expectations-ves__divition">
						<img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/circles-line-bg.png" alt="">
					</div>
				</div>
			</div>
		</div>
		</section>
		<section id="in-person" class="main-section-nopadding programs-ves">
			<div class="container-fluid">
				<div class="row">
					<h2 class="color-green">In Person</h2>
					<div class="programs-ves__content">
						<?php

						$page = array(
							'page_id' => 25,
							'post_type' => 'page',
							'post_status' => 'publish'
						);

						$my_query = new WP_Query($page);

						while ($my_query->have_posts()) : $my_query->the_post();

						$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
						$image = $image[0];

						?>
						<div class="programs-ves__content--image">
							<img src="<?= $image; ?>" alt="">
						</div>
						<?php endwhile; ?>
						<div class="programs-ves__content--list background-person">
							<?php include_once('template-parts/in-person-selection.php'); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="online" class="main-section-nopadding programs-ves">
			<div class="container-fluid">
				<div class="row">
					<h2 class="color-purple">Online</h2>
					<div class="programs-ves__content">
						<?php

							$page = array(
								'page_id' => 116,
								'post_type' => 'page',
								'post_status' => 'publish'
							);

							$my_query = new WP_Query($page);

							while ($my_query->have_posts()) : $my_query->the_post();

							$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
							$image = $image[0];

						?>
						<div class="programs-ves__content--image">
							<img src="<?= $image; ?>" alt="">
						</div>
						<?php endwhile; ?>
						<div class="programs-ves__content--list background-online">
							<?php include('template-parts/online-selection.php'); ?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="teachers" class="main-section teachers-ves">
			<div class="container-fluid">
				<div class="row">
				<div class="teachers-ves__content">
					<div class="teachers-ves__inner">
						<h2 class="text-center color-yellow">Our <br>Teachers</h2>
						<div class="teachers-ves__carousel">
						<?php 

							$query = new WP_Query(array(
								'post_type' => 'ourteachers',
								'posts_per_page' => -1
							));

							while ($query->have_posts()) : $query->the_post();

							$type = get_post_meta( $post->ID, 'teacher_type', true);
							$type = get_term($type);

							$country = get_post_meta( $post->ID, 'teacher_contry', true);
							$country = get_term($country);

							$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
							$image = $image[0];

							if($type->name == 'Teacher'){
							?>

							<div class="teachers-ves__carousel--item">
							<div class="teachers-ves__image">
								<div class="teachers-ves__image--bg" style="background-image: url('<?= $image; ?>')"></div>
							</div>
							<div class="teachers-ves__description">
								<h3 class="text-center color-green"><?= the_title(); ?></h3>
								<div class="teachers-ves__description--country">
									<img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/<?= $country->slug; ?>.png" alt="">
									<p class="text-center color-gray"><?= $country->name; ?></p>
								</div>
								
							</div>
							</div>

							<?php } endwhile; ?>
						</div>
						<div class="teachers-ves__text">
							<p class="text-left color-gray">Our teachers are <strong class="color-green">Spanish speakers</strong> with a love for <strong class="color-green">teaching children</strong>. They are also professionals in their fields, with impressive skills as <strong class="color-green">artists</strong>, dancers, musicians and educational specialists.</p>
							<p class="text-left color-gray">This broad experience <strong class="color-green">contributes</strong> to our creative learning environment. They come from different countries throughout the <strong class="color-green">Spanish-speaking world</strong>, which creates a diversity that adds a special cultural dimension to our program.</p>
						</div>
						
					</div>
					<div class="teachers-ves__cta">
							<a class="btn bordered centered background-rose-dark background-hover-rose-dark color-white" href="">Employment Application</a>
						</div>

			</div>
				</div>
			</div>	
		</section>

		<section id="teachers" class="main-section-nopadding resources-ves">
			<div class="container-fluid">
				<div class="row">
					<div class="resources-ves__content">
						<div class="container-max-dk">
							<div class="resources-ves__header">
								<h2 class="text-center color-green">Resources</h2>
							</div>
							<div class="resources-ves__body">
								<ul class="resources-ves__list">
									<?php 

										# Parent ID
										$parent_id = 170;

										# Arguments for the query
										$args = array(
											'post_parent' => $parent_id,
											'post_type'   => 'page', 
											'posts_per_page' => -1,
											'post_status' => 'publish' 
											); 

										# The parent's children
										$resources = get_children( $args );

										# Current pages ID
										$page_ID = get_the_ID();

										# Start iterating from 1, to skip first child
										foreach( $resources as $resource ) {
											$id = $resource->ID;
											$url = get_permalink( $id );
											$title = $resource->post_title;
	
									?>
									<li><a href="<?= $url; ?>" class="resources-ves__link"><?= $title; ?></a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="resources-ves__separator"></div>
				</div>
			</div>
		</section>

		<section id="gallery" class="main-section-nopadding gallery-ves">
			<div class="container-fluid">
				<div class="row">
					<div class="gallery-ves__content">
					<div class="gallery-ves__title">
						<h2 class="text-center color-blue">Gallery</h2>
					</div>
					<div class="gallery-ves__wrapper">
						<div class="gallery-ves__carousel">
							<?php 

								$query = new WP_Query(array(
									'post_type' => 'gallery',
									'posts_per_page' => -1
								));

								while ($query->have_posts()) : $query->the_post();

								$gallery = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
								$gallery = $gallery[0];

							?>
							<div class="gallery-ves__carousel--item">
								<img src="<?= $gallery; ?>" alt="">
							</div>
							<?php endwhile; ?>
		
						</div>
					</div>
					</div>			
				</div>
			</div>
	</section>



	</div>
	<div id="primary">
		<main id="main" class="site-main mt-5" role="main">
			<div class="home-page-wrap">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

					endwhile;
					?>

				<?php

				else :

					get_template_part( 'template-parts/content-none' );

				endif;
				get_template_part( 'template-parts/components/posts-carousel' );
				?>
			</div>
		</main>
	</div>

<?php
get_footer(); ?>

<script>
  // 2. This code loads the IFrame Player API code asynchronously.
  var tag = document.createElement('script');

  tag.src = "http://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // 3. This function creates an <iframe> (and YouTube player)
  //    after the API code downloads.
  var player;
  function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		videoId: 'IP7LmSVlLvs',
		playerVars: {
		autoplay: 1,
		mute: 1,
		controls: 0,
		modestbranding: 1,
		loop: 1,
		},
		events: {
			'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
	});
  }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
		  //console.log('ready');
        //event.target.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      function onPlayerStateChange(event) {
		console.log(event.data);
		if(event.data == 0){
			player.playVideo();
		}
      }



  </script>