<?php /* Template Name: Online Resources */ 

get_header();

rewind_posts();

?>

<section class="main-section-nopadding online-resources-ves">
    <div class="container-fluid">
        <div class="row">
            <div class="online-resources__title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="online-resources__content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<?php 
    get_footer();