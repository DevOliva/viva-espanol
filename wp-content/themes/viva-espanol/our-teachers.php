<?php /* Template Name: Our Teachers */

get_header();

?>

<section class="main-section teachers-ves" id="our-teachers">
    <div class="container-fluid">
        <div class="row">
        <div class="teachers-ves__content">
            <div class="teachers-ves__main">
                <div class="teachers-ves__main--title">
                <h2 class="text-center color-rose-dark">Our <br/>Teachers</h2>
                </div>
                <div class="teachers-ves__main--content">
                    <?php
                    
                        $query = new WP_Query(array(
                            'post_type' => 'ourteachers',
                            'posts_per_page' => -1
                        ));


                        while ($query->have_posts()) : $query->the_post();

                        $type = get_post_meta( $post->ID, 'teacher_type', true);
                        $type = get_term($type);
           
                        if($type->name == 'VES Employee'){
                            $position = get_post_meta( $post->ID, 'teacher_position', true);
                            $position = get_term($position);

                            $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
                            $image = $image[0];

                            $priority = get_post_meta( $post->ID, 'teacher_priority', true);

                            $modalName = get_the_title();
                            $modalName = strtolower(str_replace(' ', '-', $modalName));

                
                    ?>

                    <div class="teachers-ves__main--item">
                        <div class="teachers-ves__main--image <?= $priority == 1 ? 'bg-yellow' : 'bg-rose-dark' ?>">
                            <a class="modal-open" data-modal="#<?= $modalName; ?>" href="">
                                <img src="<?= $image; ?>" alt="">
                            </a>        
                        </div>
                        <div class="teachers-ves__main--description">
                            <a class="modal-open" data-modal="#<?= $modalName; ?>" href="">
                                <h3 class="text-center"><?= the_title(); ?></h3>
                                <p class="text-center"><?= $position->name; ?></p>
                            </a>    
                        </div>
                        
                    </div>

                    <?php } endwhile; ?>

                </div>
                
            </div>
					<div class="teachers-ves__inner">
						<div class="teachers-ves__carousel">
                            <?php 

                                while ($query->have_posts()) : $query->the_post();

                                $type = get_post_meta( $post->ID, 'teacher_type', true);
                                $type = get_term($type);

                                $country = get_post_meta( $post->ID, 'teacher_contry', true);
                                $country = get_term($country);

                                $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
                                $image = $image[0];
                
                                if($type->name == 'Teacher'){
                            ?>

							<div class="teachers-ves__carousel--item">
								<div class="teachers-ves__image">
									<div class="teachers-ves__image--bg" style="background-image: url('<?= $image; ?>')"></div>
								</div>
								<div class="teachers-ves__description">
                                    <h3 class="text-center color-green"><?= the_title(); ?></h3>
                                    <div class="teachers-ves__description--country">
                                        <img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/<?= $country->slug; ?>.png" alt="">
                                        <p class="text-center color-gray"><?= $country->name; ?></p>
                                    </div>
									
								</div>
                            </div>

                            <?php } endwhile; ?>

							<!--div class="teachers-ves__carousel--item">
								<div class="teachers-ves__image  color-green">
								<div class="teachers-ves__image--bg"></div>
								</div>
								<div class="teachers-ves__description">
									<h3 class="text-center  color-green">Cesár Martinez</h3>
									<p class="text-center color-gray">México</p>
								</div>
							</div>
							<div class="teachers-ves__carousel--item">
								<div class="teachers-ves__image  color-green">
								<div class="teachers-ves__image--bg"></div>
								</div>
								<div class="teachers-ves__description">
									<h3 class="text-center  color-green">Cesár Martinez</h3>
									<p class="text-center color-gray">México</p>
								</div>
							</div>
							<div class="teachers-ves__carousel--item">
								<div class="teachers-ves__image">
								<div class="teachers-ves__image--bg"></div>
								</div>
								<div class="teachers-ves__description">
									<h3 class="text-center  color-green">Cesár Martinez</h3>
									<p class="text-center color-gray">México</p>
								</div>
							</div>
							<div class="teachers-ves__carousel--item">
								<div class="teachers-ves__image">
								<div class="teachers-ves__image--bg"></div>
								</div>
								<div class="teachers-ves__description">
									<h3 class="text-center  color-green">Cesár Martinez</h3>
									<p class="text-center color-gray">México</p>
								</div>
							</div-->
						</div>
						<div class="teachers-ves__text">
							<p class="text-left color-gray">Our teachers are <strong class="color-green">Spanish speakers</strong> with a love for <strong class="color-green">teaching children</strong>. They are also professionals in their fields, with impressive skills as <strong class="color-green">artists</strong>, dancers, musicians and educational specialists.</p>
                            <p class="text-left color-gray">This broad experience <strong class="color-green">contributes</strong> to our creative learning environment. They come from different countries throughout the <strong class="color-green">Spanish-speaking world</strong>, which creates a diversity that adds a special cultural dimension to our program.</p>
						</div>
						
					</div>
					<div class="teachers-ves__cta">
                            <p class="color-gray"><strong><span class="color-orange">V</span><span class="color-blue">i</span><span class="color-rose-dark">v</span><span class="color-purple">a </span><span class="color-yellow">e</span><span class="color-green">l </span><span class="color-rose-dark">E</span><span class="color-purple">s</span><span class="color-green">p</span><span class="color-blue">a</span><span class="color-purple">ñ</span><span class="color-rose">o</span><span class="color-green">l</span></strong> is growing!  We are currently hiring teachers and teacher’s aides.</p>
							<a class="btn bordered centered background-rose-dark background-hover-rose-dark color-white" href="https://docs.google.com/forms/d/e/1FAIpQLSdwBzm1G6rVCdZps2HiZcvgmz5BG9FYG4OstB-XNVndOZLcFw/viewform" target="_blank">Employment Application</a>
						</div>

			</div>
        </div>
    </div>
</section>

<?php 

while ($query->have_posts()) : $query->the_post();

                        $type = get_post_meta( $post->ID, 'teacher_type', true);
                        $type = get_term($type);
           
                        if($type->name == 'VES Employee'){

                            $modalName = get_the_title();
                            $modalName = strtolower(str_replace(' ', '-', $modalName));

?>

<div id="<?= $modalName; ?>" class="modal modal-teachers" tabindex="-1" role="dialog">
<div class="modal-bg"></div>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="text-center color-white"><?= the_title(); ?></h5>
            <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                <i class="fas fa-times"></i>
            </button>
      </div>
      <div class="modal-body">
       <?= the_content(); ?>
      </div>
    </div>
  </div>
</div>

<?php } endwhile; ?>

<?php
get_footer();
