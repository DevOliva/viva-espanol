<?php 
if (have_posts()) : while (have_posts()) : the_post();

$whatINeed = explode('/', $_SERVER['REQUEST_URI']);
$whatINeed = $whatINeed[1];

if(get_the_title() !== 'Classess') { 
    $title = '- '.get_the_title(); 
}else{ 
    $title =  ''; 
}

?>


<ul>
    <li>
        <div class="custom-select">
            
            <select>
               
                <option value="0">Classes <?= $whatINeed == 'classes' ? $title : ''; ?></option>
                <?php

                $query = new WP_Query(array(
                    'post_type' => 'classes',
                    'posts_per_page' => -1
                ));


                while ($query->have_posts()) : $query->the_post();

                ?>


                    <option value="<?= get_the_permalink(); ?>"><?= the_title(); ?></option>
                <?php endwhile; ?>

            </select>
        </div>
    </li>
    <li>
        <div class="custom-select">
            <select>
                <option value="0">Camps <?= $whatINeed == 'camps' ? $title : ''; ?></option>
                <?php

                $query1 = new WP_Query(array(
                    'post_type' => 'camps',
                    'posts_per_page' => -1
                ));


                while ($query1->have_posts()) : $query1->the_post();

                ?>

                    <option value="<?= get_the_permalink(); ?>"><?= the_title(); ?></option>
                    
                <?php endwhile; ?>

            </select>
        </div>
    </li>
    <li>
        <a href="<?php echo get_home_url(); ?>/in-person/events">Events</a>
    </li>
    <li>
    <a href="<?php echo get_home_url(); ?>/in-person/private-tutoring">Private tutoring</a>
    </li>
</ul>
<?php endwhile;
endif; ?>