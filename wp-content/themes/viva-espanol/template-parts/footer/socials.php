<?php
/**
 * Header Navigation template.
 *
 * @package VES
 */

$menu_class = \VES_Theme\Inc\Menus::get_instance();
$header_menu_id = $menu_class->get_menu_id( 'ves-footer-socials-menu' );
$header_menus = wp_get_nav_menu_items( $header_menu_id );

?>

<?php
			if ( ! empty( $header_menus ) && is_array( $header_menus ) ) {
                ?>
<ul class="footer-list__rrss">

                <?php
					foreach ( $header_menus as $menu_item ) {

                        if ( ! $menu_item->menu_item_parent ) {
                        ?>
					<li>
						<a class="<?php echo esc_html( $menu_item->title ); ?>" href="<?php echo esc_url( $menu_item->url ); ?>" target="_blank"></a>
					</li>
					<!--li>
						<a class="instagram-footer" href="#"></a>
					</li>
					<li>
						<a class="yelp-footer" href="#"></a>
					</li>
					<li>
						<a class="youtube-footer" href="#"></a>
                    </li-->
                    <?php } } ?>
                </ul>
                <?php } ?>