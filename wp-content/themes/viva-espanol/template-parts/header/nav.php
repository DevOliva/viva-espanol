<?php
/**
 * Header Navigation template.
 *
 * @package VES
 */

$menu_class = \VES_Theme\Inc\Menus::get_instance();
$header_menu_id = $menu_class->get_menu_id( 'ves-header-menu' );
$header_menus = wp_get_nav_menu_items( $header_menu_id );

?>

<nav class="site-nav">
	<div class="site-nav__left">
		<?php
		if ( function_exists( 'the_custom_logo' ) ) {
			the_custom_logo();
		}
		?>
	</div>
	<div class="site-nav__right">
		<button class="nav-toggle">
			<span class="nav-toggle__icon"></span>
		</button>

		<div class="site-menu">
			<?php
			if ( ! empty( $header_menus ) && is_array( $header_menus ) ) {

				?>
				<ul class="site-menu__nav">
					<?php
					
					foreach ( $header_menus as $menu_item ) {
			
					
						if ( ! $menu_item->menu_item_parent ) {
							// Compare menu object with current page menu object
							$current = ( $menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
							$child_menu_items = $menu_class->get_child_menu_items( $header_menus, $menu_item->ID );
							$has_children = ! empty( $child_menu_items ) && is_array( $child_menu_items );
							$has_sub_menu_class = ! empty( $has_children ) ? 'has-submenu' : '';
							
							if ( ! $has_children ) {

								?>
								<li class="site-menu__item" data-name="<?php echo strtolower(str_replace(' ', '-', $menu_item->title)) ?>">
									<a id="menu-<?= $menu_item->ID; ?>" class="site-menu__link <?= $current; ?>" href="<?php echo esc_url( $menu_item->url ); ?>">
										<?php echo esc_html( $menu_item->title ); ?>
									</a>
								</li>
								<?php
							} else {					
	
								?>
								<li class="site-menu__item has-submenu" data-name="<?php echo strtolower(str_replace(' ', '-', $menu_item->title)) ?>">
									<a id="menu-<?= $menu_item->ID; ?>" class="site-menu__link <?php 
									echo $current;
									foreach ( $child_menu_items as $child_menu_item ) { 
										if ($child_menu_item->object_id == get_queried_object_id()){ 
											echo 'current';
										}else{
											echo'';
										}
									} 
									?> <?php echo esc_html( $menu_item->classes[0] ); ?>" href="<?php echo esc_url( $menu_item->url ); ?>">
										<?php echo esc_html( $menu_item->title ); ?>
									</a>
									<div class="site-menu__dropdown">
										<?php
										foreach ( $child_menu_items as $child_menu_item ) {
											// Compare menu object with current page menu object
											$current_children = ( $child_menu_item->object_id == get_queried_object_id() ) ? 'current' : '';
											?>
											<a id="menu-<?= $child_menu_item->ID; ?>" class="site-menu__link <?= $current_children; ?><?php echo esc_html( $child_menu_item->classes[0] ); ?>" href="<?php echo esc_url( $child_menu_item->url ); ?>">
												<?php echo esc_html( $child_menu_item->title ); ?>
											</a>
											<?php
										}
										?>
									</div>
								</li>
								<?php
							}
							?>
							<?php
						}
					}
					?>
				</ul>
				<?php
			}
			?>
			<!--form class="form-inline my-2 my-lg-0">
				<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form-->
		</div>

	</div>




</nav>
