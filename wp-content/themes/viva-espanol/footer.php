<?php
/**
 * Footer template
 *
 * @package Viva el español
 */
?>

<button class="backto-top">
	<i class="fas fa-chevron-up"></i>
</button>
<footer>
	<!-- <h3>Footer</h3> -->
	<?php if ( is_active_sidebar( 'sidebar-2' ) ) { ?>
		<!-- <aside>
			<?php dynamic_sidebar( 'sidebar-2' ); ?>
		</aside> -->
	<?php } ?>

	<div class="container-fluid">
		<div class="footer-row row">
			<div class="col-12 order-1 col-sm-6 order-sm-1 col-md-4 order-md-1 footer-item">
				<?php dynamic_sidebar( 'footer_area_one' ); ?>
				<!--h3 class="footer-title text-left">Subscribe to Our Newsletter</h3>
				<p class="footer-text text-left">Stay current with our latest events!</p>
				<p class="footer-text text-left">Sign up for our Email Newsletter today:</p>
				<div class="footer-newsletter">
					<input type="text" name="" value="">
					<button class="btn bg-green bordered medium" type="submit">Send</button>
				</div-->
			</div>
			<div class="col-12 order-3 col-sm-12 order-sm-3 col-md-5 order-md-2 footer-item">
				<h3 class="footer-title background-text background-green-dark text-centered text-center">Viva el Español Awards</h3>
				<ul class="footer-list__awards">
					<li><img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/2015-Silver-medal.png" /></li>
					<li><img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/2016-transparent-png.png" /></li>
					<li><img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/2019-best-of.png" /></li>
					<li><img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/2020ParentsPressMedal_Winner.png" /></li>
				</ul>
			</div>
			<div class="col-12 order-2 col-sm-6 order-sm-2 col-md-3 order-md-3 footer-item">
				<?php dynamic_sidebar( 'footer_area_two' ); ?>
				<!-- <h3 class="footer-title text-right">Contact Information</h3>
				<p class="footer-text text-right">Phone: <a href="tel:9259629177">925-962-9177</a></p>
				<p class="footer-text text-right">Email: <a href="mailto:info@vivaelespanol.org">info@vivaelespanol.org</a></p> -->
			</div>
		</div>
		<div class="footer-row row">
			<div class="col-12 col-sm-6 col-md-4 footer-item">
				<a class="site-logo" href="<?php echo get_home_url(); ?>">
					<img src="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/logo-white.svg" alt="">
				</a>
			</div>
			<div class="col-12 col-sm-6 col-md-3 footer-item d-none d-md-block">
			</div>
			<div class="col-12 col-sm-6 col-md-5 footer-item">
			<?php get_template_part( 'template-parts/footer/socials' ); ?>
				<!-- <ul class="footer-list__rrss">
					<li>
						<a class="facebook-footer" href="#"></a>
					</li>
					<li>
						<a class="instagram-footer" href="#"></a>
					</li>
					<li>
						<a class="yelp-footer" href="#"></a>
					</li>
					<li>
						<a class="youtube-footer" href="#"></a>
					</li>
				</ul> -->
			</div>
		</div>
	</div>

</footer>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
