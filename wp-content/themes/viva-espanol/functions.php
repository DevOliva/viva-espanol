<?php

if (!defined('VES_DIR_PATH')) {
	define('VES_DIR_PATH', untrailingslashit(get_template_directory()));
}

if (!defined('VES_DIR_URI')) {
	define('VES_DIR_URI', untrailingslashit(get_template_directory_uri()));
}

if (!defined('VES_DIST_URI')) {
	define('VES_DIST_URI', untrailingslashit(get_template_directory_uri()) . '/src/dist');
}

if (!defined('VES_DIST_URI_PATH')) {
	define('VES_DIST_URI', untrailingslashit(get_template_directory()) . '/src/dist');
}

if (!defined('VES_DIST_JS_URI')) {
	define('VES_DIST_JS_URI', untrailingslashit(get_template_directory_uri()) . '/src/dist/assets/js');
}

if (!defined('VES_DIST_JS_URI_PATH')) {
	define('VES_DIST_JS_URI', untrailingslashit(get_template_directory()) . '/src/dist/assets/js');
}

if (!defined('VES_DIST_IMG_URI')) {
	define('VES_DIST_IMG_URI', untrailingslashit(get_template_directory_uri()) . '/src/dist/assets/img');
}

if (!defined('VES_DIST_CSS_URI')) {
	define('VES_DIST_CSS_URI', untrailingslashit(get_template_directory_uri()) . '/src/dist/assets/css');
}

if (!defined('VES_BUILD_CSS_DIR_PATH')) {
	define('VES_BUILD_CSS_DIR_PATH', untrailingslashit(get_template_directory()) . '/src/dist/assets/css');
}

if (!defined('VES_DIST_LIB_URI')) {
	define('VES_DIST_LIB_URI', untrailingslashit(get_template_directory_uri()) . '/src/dist/library');
}

require_once VES_DIR_PATH . '/inc/helpers/autoloader.php';
require_once VES_DIR_PATH . '/inc/helpers/template-tags.php';

function ves_get_theme_instance()
{
	\VES_THEME\Inc\VES_THEME::get_instance();
}

ves_get_theme_instance();

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

add_post_type_support( 'page', 'excerpt' );

add_filter('rwmb_meta_boxes', 'prefix_register_metaboxes');

function about_us_content($content)
{

	global $post;

	if($post->post_type == 'page' && $post->ID == '200'){
		
		$content = str_replace('<li aria-level="1">', '<li><svg viewBox="0 0 305.9 311.5"><path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path></svg>', $content);

		return $content;
	}

	if ($post->post_type == 'page' && $post->ID == '20') {

		$content = str_replace('<ul>', '<ul class="about-ves__list">', $content);
		$content = str_replace('<li>', '<li class="about-ves__list--item"><svg viewBox="0 0 305.9 311.5"><path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path></svg>', $content);

		return $content;
	} else if($post->post_type == 'page' && $post->ID == '172'){
		$content = str_replace('<li>', '<li><svg viewBox="0 0 305.9 311.5"><path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path></svg>', $content);

		return $content;

	} else if($post->post_type == 'camps'){
		$content = str_replace('<li>', '<li><svg viewBox="0 0 305.9 311.5"><path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path></svg>', $content);

		return $content;

	} else if($post->post_type == 'page' && !is_page_template( 'articles.php' )){
		$content = str_replace('<li>', '<li><svg viewBox="0 0 305.9 311.5"><path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path></svg>', $content);

		return $content;

	} else if($post->post_type == 'page' && $post->ID == '200'){
	
		$content = str_replace('<li aria-level="1">', '<li><svg viewBox="0 0 305.9 311.5"><path class="st0" d="M0,153.1L0,153.1c0,34.5,11.8,68,33.3,94.9L11,299.9c-1.8,4.3,0.2,9.2,4.3,10.9c1.5,0.7,3.2,0.8,4.8,0.6 l81.7-14.4c16.4,5.9,33.5,8.9,50.9,8.8c84.5,0,153.1-68.7,153.1-153.1C306,68.5,237.9,0.1,153.7,0h-0.5C68.7,0,0,68.7,0,153.1z M101.2,122.3h45.1c4.6,0,8.3,3.8,8.3,8.3c0,4.6-3.8,8.3-8.3,8.3h-45.1c-4.6,0-8.3-3.8-8.3-8.3C92.9,126.1,96.6,122.3,101.2,122.3z M213.1,172.4c0,4.6-3.8,8.3-8.3,8.3H101.2c-4.6,0-8.3-3.8-8.3-8.3c0-4.6,3.8-8.3,8.3-8.3h103.5 C209.3,164.1,213.1,167.8,213.1,172.4z"></path></svg>', $content);

		return $content;

	} else {
		return $content;
	}
}

add_filter('the_content', 'about_us_content');

// // Lo enganchamos en la acción init y llamamos a la función create_book_taxonomies() cuando arranque
// add_action( 'init', 'create_book_taxonomies', 0 );  

// // Creamos dos taxonomías, género y autor para el custom post type "libro"
// function create_book_taxonomies() {
//   /* Configuramos las etiquetas que mostraremos en el escritorio de WordPress */
//   $labels = array(
//     'name'             => _x( 'Hours', 'taxonomy general name' ),
//     'singular_name'    => _x( 'Hours', 'taxonomy singular name' ),
//     'search_items'     =>  __( 'Search by Hours' ),
//     'all_items'        => __( 'All hours' ),
//     'parent_item'      => __( 'Hours parent' ),
//     'parent_item_colon'=> __( 'Hours parent' ),
//     'edit_item'        => __( 'Edit Hours' ),
//     'update_item'      => __( 'Update Hours' ),
//     'add_new_item'     => __( 'Add new Hours' ),
//     'new_item_name'    => __( 'Name of the Hour' ),
//   );

//   /* Registramos la taxonomía y la configuramos como jerárquica (al estilo de las categorías) */
//   register_taxonomy( 'genero', array( 'classess' ), array(
//     'hierarchical'       => true,
//     'labels'             => $labels,
//     'show_ui'            => true,
//     'query_var'          => true,
//     'rewrite'            => array( 'slug' => 'hours' ),
//   ));

// }

add_action('init', 'your_prefix_register_taxonomy');
function your_prefix_register_taxonomy()
{
	$args = [
		'label'  => esc_html__('Hours', 'your-textdomain'),
		'labels' => [
			'menu_name'                  => esc_html__('Hours', 'your-textdomain'),
			'all_items'                  => esc_html__('All Hours', 'your-textdomain'),
			'edit_item'                  => esc_html__('Edit hour', 'your-textdomain'),
			'view_item'                  => esc_html__('View hour', 'your-textdomain'),
			'update_item'                => esc_html__('Update hour', 'your-textdomain'),
			'add_new_item'               => esc_html__('Add new hour', 'your-textdomain'),
			'new_item'                   => esc_html__('New hour', 'your-textdomain'),
			'parent_item'                => esc_html__('Parent hour', 'your-textdomain'),
			'parent_item_colon'          => esc_html__('Parent hour', 'your-textdomain'),
			'search_items'               => esc_html__('Search Hours', 'your-textdomain'),
			'popular_items'              => esc_html__('Popular Hours', 'your-textdomain'),
			'separate_items_with_commas' => esc_html__('Separate Hours with commas', 'your-textdomain'),
			'add_or_remove_items'        => esc_html__('Add or remove Hours', 'your-textdomain'),
			'choose_from_most_used'      => esc_html__('Choose most used Hours', 'your-textdomain'),
			'not_found'                  => esc_html__('No Hours found', 'your-textdomain'),
			'name'                       => esc_html__('Hours', 'your-textdomain'),
			'singular_name'              => esc_html__('hour', 'your-textdomain'),
		],
		'public'               => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_tagcloud'        => true,
		'show_in_quick_edit'   => true,
		'show_admin_column'    => false,
		'show_in_rest'         => true,
		'hierarchical'         => false,
		'query_var'            => true,
		'sort'                 => false,
		'rewrite_no_front'     => false,
		'rewrite_hierarchical' => false,
		'rewrite' => true
	];

	$args1 = [
		'label'  => esc_html__('Day', 'your-textdomain'),
		'labels' => [
			'menu_name'                  => esc_html__('Days', 'your-textdomain'),
			'all_items'                  => esc_html__('All Day', 'your-textdomain'),
			'edit_item'                  => esc_html__('Edit Day', 'your-textdomain'),
			'view_item'                  => esc_html__('View Day', 'your-textdomain'),
			'update_item'                => esc_html__('Update Day', 'your-textdomain'),
			'add_new_item'               => esc_html__('Add new Day', 'your-textdomain'),
			'new_item'                   => esc_html__('New Day', 'your-textdomain'),
			'parent_item'                => esc_html__('Parent Day', 'your-textdomain'),
			'parent_item_colon'          => esc_html__('Parent Day', 'your-textdomain'),
			'search_items'               => esc_html__('Search Day', 'your-textdomain'),
			'popular_items'              => esc_html__('Popular Day', 'your-textdomain'),
			'separate_items_with_commas' => esc_html__('Separate Day with commas', 'your-textdomain'),
			'add_or_remove_items'        => esc_html__('Add or remove Day', 'your-textdomain'),
			'choose_from_most_used'      => esc_html__('Choose most used Day', 'your-textdomain'),
			'not_found'                  => esc_html__('No Day found', 'your-textdomain'),
			'name'                       => esc_html__('Day', 'your-textdomain'),
			'singular_name'              => esc_html__('Day', 'your-textdomain'),
		],
		'public'               => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_tagcloud'        => true,
		'show_in_quick_edit'   => true,
		'show_admin_column'    => false,
		'show_in_rest'         => true,
		'hierarchical'         => false,
		'query_var'            => true,
		'sort'                 => false,
		'rewrite_no_front'     => false,
		'rewrite_hierarchical' => false,
		'rewrite' => true
	];

	$args2 = [
		'label'  => esc_html__('Class', 'your-textdomain'),
		'labels' => [
			'menu_name'                  => esc_html__('Class', 'your-textdomain'),
			'all_items'                  => esc_html__('All Class', 'your-textdomain'),
			'edit_item'                  => esc_html__('Edit Class', 'your-textdomain'),
			'view_item'                  => esc_html__('View Class', 'your-textdomain'),
			'update_item'                => esc_html__('Update Class', 'your-textdomain'),
			'add_new_item'               => esc_html__('Add new Class', 'your-textdomain'),
			'new_item'                   => esc_html__('New Class', 'your-textdomain'),
			'parent_item'                => esc_html__('Parent Class', 'your-textdomain'),
			'parent_item_colon'          => esc_html__('Parent Class', 'your-textdomain'),
			'search_items'               => esc_html__('Search Class', 'your-textdomain'),
			'popular_items'              => esc_html__('Popular Class', 'your-textdomain'),
			'separate_items_with_commas' => esc_html__('Separate Class with commas', 'your-textdomain'),
			'add_or_remove_items'        => esc_html__('Add or remove Class', 'your-textdomain'),
			'choose_from_most_used'      => esc_html__('Choose most used Class', 'your-textdomain'),
			'not_found'                  => esc_html__('No Class found', 'your-textdomain'),
			'name'                       => esc_html__('Class', 'your-textdomain'),
			'singular_name'              => esc_html__('Class', 'your-textdomain'),
		],
		'public'               => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_tagcloud'        => true,
		'show_in_quick_edit'   => true,
		'show_admin_column'    => false,
		'show_in_rest'         => true,
		'hierarchical'         => false,
		'query_var'            => true,
		'sort'                 => false,
		'rewrite_no_front'     => false,
		'rewrite_hierarchical' => false,
		'rewrite' => true
	];

	$args3 = [
		'label'  => esc_html__('Time', 'your-textdomain'),
		'labels' => [
			'menu_name'                  => esc_html__('Time', 'your-textdomain'),
			'all_items'                  => esc_html__('All Time', 'your-textdomain'),
			'edit_item'                  => esc_html__('Edit Time', 'your-textdomain'),
			'view_item'                  => esc_html__('View Time', 'your-textdomain'),
			'update_item'                => esc_html__('Update Time', 'your-textdomain'),
			'add_new_item'               => esc_html__('Add new Time', 'your-textdomain'),
			'new_item'                   => esc_html__('New Time', 'your-textdomain'),
			'parent_item'                => esc_html__('Parent Time', 'your-textdomain'),
			'parent_item_colon'          => esc_html__('Parent Time', 'your-textdomain'),
			'search_items'               => esc_html__('Search Time', 'your-textdomain'),
			'popular_items'              => esc_html__('Popular Time', 'your-textdomain'),
			'separate_items_with_commas' => esc_html__('Separate Time with commas', 'your-textdomain'),
			'add_or_remove_items'        => esc_html__('Add or remove Time', 'your-textdomain'),
			'choose_from_most_used'      => esc_html__('Choose most used Time', 'your-textdomain'),
			'not_found'                  => esc_html__('No Time found', 'your-textdomain'),
			'name'                       => esc_html__('Time', 'your-textdomain'),
			'singular_name'              => esc_html__('Time', 'your-textdomain'),
		],
		'public'               => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_tagcloud'        => true,
		'show_in_quick_edit'   => true,
		'show_admin_column'    => false,
		'show_in_rest'         => true,
		'hierarchical'         => false,
		'query_var'            => true,
		'sort'                 => false,
		'rewrite_no_front'     => false,
		'rewrite_hierarchical' => false,
		'rewrite' => true
	];

	$args4 = [
		'label'  => esc_html__( 'Types', 'text_domain' ),
		'labels' => [
			'menu_name'                  => esc_html__( 'Types', 'text_domain' ),
			'all_items'                  => esc_html__( 'All Types', 'text_domain' ),
			'edit_item'                  => esc_html__( 'Edit Type', 'text_domain' ),
			'view_item'                  => esc_html__( 'View Type', 'text_domain' ),
			'update_item'                => esc_html__( 'Update Type', 'text_domain' ),
			'add_new_item'               => esc_html__( 'Add new Type', 'text_domain' ),
			'new_item'                   => esc_html__( 'New Type', 'text_domain' ),
			'parent_item'                => esc_html__( 'Parent Type', 'text_domain' ),
			'parent_item_colon'          => esc_html__( 'Parent Type', 'text_domain' ),
			'search_items'               => esc_html__( 'Search Types', 'text_domain' ),
			'popular_items'              => esc_html__( 'Popular Types', 'text_domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Types with commas', 'text_domain' ),
			'add_or_remove_items'        => esc_html__( 'Add or remove Types', 'text_domain' ),
			'choose_from_most_used'      => esc_html__( 'Choose most used Types', 'text_domain' ),
			'not_found'                  => esc_html__( 'No Types found', 'text_domain' ),
			'name'                       => esc_html__( 'Types', 'text_domain' ),
			'singular_name'              => esc_html__( 'Type', 'text_domain' ),
		],
		'public'               => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_tagcloud'        => true,
		'show_in_quick_edit'   => true,
		'show_admin_column'    => false,
		'show_in_rest'         => true,
		'hierarchical'         => false,
		'query_var'            => true,
		'sort'                 => false,
		'rewrite_no_front'     => false,
		'rewrite_hierarchical' => false,
		'rewrite' => true
	];

	$args5 = [
		'label'  => esc_html__( 'Positions', 'text_domain' ),
		'labels' => [
			'menu_name'                  => esc_html__( 'Positions', 'text_domain' ),
			'all_items'                  => esc_html__( 'All Positions', 'text_domain' ),
			'edit_item'                  => esc_html__( 'Edit Position', 'text_domain' ),
			'view_item'                  => esc_html__( 'View Position', 'text_domain' ),
			'update_item'                => esc_html__( 'Update Position', 'text_domain' ),
			'add_new_item'               => esc_html__( 'Add new Position', 'text_domain' ),
			'new_item'                   => esc_html__( 'New Position', 'text_domain' ),
			'parent_item'                => esc_html__( 'Parent Position', 'text_domain' ),
			'parent_item_colon'          => esc_html__( 'Parent Position', 'text_domain' ),
			'search_items'               => esc_html__( 'Search Positions', 'text_domain' ),
			'popular_items'              => esc_html__( 'Popular Positions', 'text_domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Positions with commas', 'text_domain' ),
			'add_or_remove_items'        => esc_html__( 'Add or remove Positions', 'text_domain' ),
			'choose_from_most_used'      => esc_html__( 'Choose most used Positions', 'text_domain' ),
			'not_found'                  => esc_html__( 'No Positions found', 'text_domain' ),
			'name'                       => esc_html__( 'Positions', 'text_domain' ),
			'singular_name'              => esc_html__( 'Position', 'text_domain' ),
		],
		'public'               => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_tagcloud'        => true,
		'show_in_quick_edit'   => true,
		'show_admin_column'    => false,
		'show_in_rest'         => true,
		'hierarchical'         => false,
		'query_var'            => true,
		'sort'                 => false,
		'rewrite_no_front'     => false,
		'rewrite_hierarchical' => false,
		'rewrite' => true
	];

	$args6 = [
		'label'  => esc_html__( 'Contries', 'text_domain' ),
		'labels' => [
			'menu_name'                  => esc_html__( 'Contries', 'text_domain' ),
			'all_items'                  => esc_html__( 'All Contries', 'text_domain' ),
			'edit_item'                  => esc_html__( 'Edit Country', 'text_domain' ),
			'view_item'                  => esc_html__( 'View Country', 'text_domain' ),
			'update_item'                => esc_html__( 'Update Country', 'text_domain' ),
			'add_new_item'               => esc_html__( 'Add new Country', 'text_domain' ),
			'new_item'                   => esc_html__( 'New Country', 'text_domain' ),
			'parent_item'                => esc_html__( 'Parent Country', 'text_domain' ),
			'parent_item_colon'          => esc_html__( 'Parent Country', 'text_domain' ),
			'search_items'               => esc_html__( 'Search Contries', 'text_domain' ),
			'popular_items'              => esc_html__( 'Popular Contries', 'text_domain' ),
			'separate_items_with_commas' => esc_html__( 'Separate Contries with commas', 'text_domain' ),
			'add_or_remove_items'        => esc_html__( 'Add or remove Contries', 'text_domain' ),
			'choose_from_most_used'      => esc_html__( 'Choose most used Contries', 'text_domain' ),
			'not_found'                  => esc_html__( 'No Contries found', 'text_domain' ),
			'name'                       => esc_html__( 'Contries', 'text_domain' ),
			'singular_name'              => esc_html__( 'Country', 'text_domain' ),
		],
		'public'               => true,
		'show_ui'              => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_tagcloud'        => true,
		'show_in_quick_edit'   => true,
		'show_admin_column'    => false,
		'show_in_rest'         => true,
		'hierarchical'         => false,
		'query_var'            => true,
		'sort'                 => false,
		'rewrite_no_front'     => false,
		'rewrite_hierarchical' => false,
		'rewrite' => true
	];

	register_taxonomy('hour', ['classes'], $args);
	register_taxonomy('day', ['classes'], $args1);
	register_taxonomy('class', ['online'], $args2);
	register_taxonomy('time', ['online'], $args3);
	register_taxonomy('types', ['ourteachers'], $args4);
	register_taxonomy('position', ['ourteachers'], $args5);
	register_taxonomy( 'country', [ 'ourteachers' ], $args6 );
}

// Create taxonomies

// POST TYPES, METABOXES, TAXONOMIES AND CUSTOM PAGES ////////////////////////////////
//require_once('inc/pages.php');

// Register Custom Post Type
function custom_post_type()
{

	// Home Slider
	$labelsSlider = array(
		'name'                => _x('Home Slider', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('Home Slider', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('Home Slider', 'text_domain'),
		'parent_item_colon'   => __('Parent Item:', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsSlider = array(
		'label'               => __('Slider', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsSlider,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// About us
	$labelsAbout = array(
		'name'                => _x('About us', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('About us', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('About us', 'text_domain'),
		'parent_item_colon'   => __('Parent Item:', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsAbout = array(
		'label'               => __('About us', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsAbout,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Our Programs
	$labelsPrograms = array(
		'name'                => _x('Our Programs', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('Our Programs', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('Our Programs', 'text_domain'),
		'parent_item_colon'   => __('Parent Item:', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsPrograms = array(
		'label'               => __('Our Programs', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsPrograms,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Classess
	$labelsClasses = array(
		'name'                => _x('Classes', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('Classes', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('Classes', 'text_domain'),
		'parent_item_colon'   => __('Classes', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsClasses = array(
		'label'               => __('Classes', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsClasses,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Camps
	$labelsCamps = array(
		'name'                => _x('Camps', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('Camps', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('Camps', 'text_domain'),
		'parent_item_colon'   => __('Camps', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsCamps = array(
		'label'               => __('Camps', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsCamps,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Online
	$labelsOnline = array(
		'name'                => _x('Online', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('Online', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('Online', 'text_domain'),
		'parent_item_colon'   => __('Online', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsOnline = array(
		'label'               => __('Online', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsOnline,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite' 			  => array('slug' => 'online-table'),
	);

	// Our teachers
	$labelsTeachers = array(
		'name'                => _x('Our teachers', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('Our teachers', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('Our teachers', 'text_domain'),
		'parent_item_colon'   => __('Our teachers', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsTeachers = array(
		'label'               => __('Our teachers', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsTeachers,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);

	// Gallery
	$labelsGallery = array(
		'name'                => _x('Gallery', 'Post Type General Name', 'text_domain'),
		'singular_name'       => _x('Gallery', 'Post Type Singular Name', 'text_domain'),
		'menu_name'           => __('Gallery', 'text_domain'),
		'parent_item_colon'   => __('Gallery', 'text_domain'),
		'all_items'           => __('Todos', 'text_domain'),
		'view_item'           => __('View Item', 'text_domain'),
		'add_new_item'        => __('Añadir Nuevo Item', 'text_domain'),
		'add_new'             => __('Añadir Item', 'text_domain'),
		'edit_item'           => __('Editar Item', 'text_domain'),
		'update_item'         => __('Actualizar Item', 'text_domain'),
		'search_items'        => __('Buscar Item', 'text_domain'),
		'not_found'           => __('Not found', 'text_domain'),
		'not_found_in_trash'  => __('Not found in Trash', 'text_domain'),
	);

	$argsGallery = array(
		'label'               => __('Gallery', 'text_domain'),
		'description'         => __('Post Type Description', 'text_domain'),
		'labels'              => $labelsGallery,
		'supports'            => array('title', 'editor', 'thumbnail'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-admin-multisite',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	

	// Register Home Slider
	register_post_type('Slider', $argsSlider);
	// Register Gallery
	register_post_type('Gallery', $argsGallery);
	// Register About
	register_post_type('About us', $argsAbout);
	// Register Our Programs
	register_post_type('Our Programs', $argsPrograms);
	// Register Classes
	register_post_type('Classes', $argsClasses);
	// Register Camps
	register_post_type('Camps', $argsCamps);
	// Register Online
	register_post_type('Online', $argsOnline);
	// Register Online
	register_post_type('Our teachers', $argsTeachers);
}


// Hook into the 'init' action
add_action('init', 'custom_post_type', 0);

/* Custom fields inside custom post type */

// Home carousel

function prefix_register_metaboxes($meta_boxes)
{

	$meta_boxes[] = array(
		'title' => __('Título', 'textdomain'),
		'post_types' => array('page'),
		'fields' => array(
			array(
				'name'        => 'Título',
				'label_description' => 'Título de la sección, puede ser html',
				'id'          => 'title_page',
				'type'        => 'textarea',
			),
		),
	);

	$meta_boxes[] = array(
		'title' => __('Maestros Only', 'textdomain'),
		'post_types' => array('page'),
		'fields' => array(
			array(
				'group_title' => 'Drive:',
				'id'     => 'drives_group',
				'type'   => 'group',
				'clone'  => true,
				'sort_clone' => false,
				'collapsible' => true,
				'fields' => array(
					array(
						'name' => 'Nombre del drive:',
						'id' => 'button_name',
						'type' => 'text'
					),
					array(
						'name' => 'URL del drive:',
						'id' => 'button_url',
						'type' => 'text'
					),
				),
			),
		),
	);

	/* Home Slider */
	$meta_boxes[] = array(
		'title' => __('Título', 'textdomain'),
		'post_types' => array('slider'),
		'fields' => array(
			array(
				'name'        => 'Título',
				'label_description' => 'Título del carousel, puede ser html',
				'id'          => 'title_slide',
				'type'        => 'textarea',
			),
		),
	);


	$meta_boxes[] = array(
		'title' => __('Type of slide', 'textdomain'),
		'post_types' => array('slider'),
		'fields' => array(
			array(
				'name'    => 'Title Secció',
				'id'      => 'radio_selection',
				'type'    => 'radio',
				// Array of 'value' => 'Label' pairs for radio options.
				// Note: the 'value' is stored in meta field, not the 'Label'
				'options' => array(
					'image' => 'Image',
					'video' => 'Video',
				),
			),
		),
	);

	$meta_boxes[] = array(
		'title' => __('Video', 'textdomain'),
		'post_types' => array('slider'),
		'fields' => array(
			array(
				'name'             => 'Video',
				'id'               => 'video_field',
				'type'             => 'video',

				// Maximum video uploads. 0 = unlimited.
				'max_file_uploads' => 1,

				// Delete videos from Media Library when remove it from post meta?
				// Note: it might affect other posts if you use same videos for multiple posts
				'force_delete'     => false,

				// Display the "Uploaded 1/3 videos" status
				'max_status'       => true,
			),
		),
	);




	$meta_boxes[] = array(
		'title'      => 'Botones',
		'post_types' => array('slider'),
		'fields'     => array(
			array(
				'group_title' => 'Botón:',
				'id'     => 'button_group',
				'type'   => 'group',
				'clone'  => true,
				'sort_clone' => false,
				'collapsible' => true,
				'fields' => array(
					array(
						'name' => 'Nombre del botón:',
						'id' => 'button_name',
						'type' => 'text'
					),
					array(
						'name' => 'URL del botón:',
						'id' => 'button_url',
						'type' => 'text'
					),
					array(
						'name'          => 'Color del botón',
						'label_description' => 'Elegir el color del botón, el hover action no se puede modificar',
						'id'            => 'button_color',
						'type'          => 'color',
						// Add alpha channel?
						'alpha_channel' => true,
						// Color picker options. See here: https://automattic.github.io/Iris/.
						'js_options'    => array(
							'palettes' => array('#125', '#459', '#78b', '#ab0', '#de3', '#f0f')
						),
					),
				),
			),
		),
	);

	$meta_boxes[] = array(
		'title' => __('Type of program', 'textdomain'),
		'post_types' => array('Our Programs'),
		'fields' => array(
			array(
				'name'    => 'Programs',
				'id'      => 'radio_programs',
				'type'    => 'radio',
				// Array of 'value' => 'Label' pairs for radio options.
				// Note: the 'value' is stored in meta field, not the 'Label'
				'options' => array(
					'pre-kinder' => 'Pre-K Kindergarden',
					'elementary-school' => 'Elementary School',
					'middle-school' => 'Middle School',
					'events' => 'Events',
					'camps' => 'Camps',
					'private-tutoring' => 'Private Tutoring',
					'parent-me' => 'Parent and Me'
				),
				'inline' => false
			),
		),
	);

	$meta_boxes[] = array(
		'title' => __('Location', 'textdomain'),
		'post_types' => array('Our Programs'),
		'fields' => array(
			array(
				'id'          => 'location',
				'type'        => 'text',
			),
		),
	);

	$meta_boxes[] = array(
		'title'      => 'Downloads',
		'post_types' => array('Our Programs'),
		'fields'     => array(
			array(
				'name' => 'Download button hide:',
				'id' => 'download_check',
				'type' => 'checkbox'
			),
			array(
				'name' => 'Text download intro:',
				'id' => 'download_text',
				'type' => 'wysiwyg'
			),
			array(
				'id'               => 'download_link',
				'name'             => 'Text download button',
				'type'             => 'wysiwyg'
			),
			array(
				'id'               => 'download_document',
				'name'             => 'Document',
				'type'             => 'file_upload',

				// Delete file from Media Library when remove it from post meta?
				// Note: it might affect other posts if you use same file for multiple posts
				'force_delete'     => false,

				// Maximum file uploads.
				'max_file_uploads' => 1,

				// File types.
				// 'mime_type'        => 'application,audio,video',

				// Do not show how many files uploaded/remaining.
				'max_status'       => false,
			),
			array(
				'id'               => 'download_image',
				'name'             => 'Image',
				'type'             => 'file_upload',

				// Delete file from Media Library when remove it from post meta?
				// Note: it might affect other posts if you use same file for multiple posts
				'force_delete'     => false,

				// Maximum file uploads.
				'max_file_uploads' => 1,

				// File types.
				// 'mime_type'        => 'application,audio,video',

				// Do not show how many files uploaded/remaining.
				'max_status'       => false,
			),
		),
	);

	$meta_boxes[] = array(
		'title'      => 'Quitar tabla',
		'post_types' => array('classes'),
		'fields' => array(
			array(
				'name' => 'Bloqueo de tabla:',
				'id' => 'block_page',
				'type' => 'checkbox',
			),
		),
	);

	$meta_boxes[] = array(
		'title'      => 'Calendar',
		'post_types' => array('classes'),
		'fields'     => array(

			array(
				'group_title' => 'Day:',
				'id'     => 'calendar_group',
				'type'   => 'group',
				'clone'  => true,
				'sort_clone' => true,
				'collapsible' => true,
				'fields' => array(
					array(
						'name' => 'Day name:',
						'id' => 'day',
						'type'       => 'taxonomy_advanced',
						'name'       => 'Day',
						'id'         => 'day_selection',
						'taxonomy'   => 'day',
						'field_type' => 'select',
					),
					array(
						'group_title' => 'Activities:',
						'id'     => 'calendar_activities',
						'type'   => 'group',
						'clone'  => true,
						'sort_clone' => true,
						'collapsible' => true,
						'fields' => array(
							array(
								'type'       => 'taxonomy_advanced',
								'name'       => 'Hour',
								'id'         => 'hour_selection',
								'taxonomy'   => 'hour',
								'field_type' => 'select',
							),
							array(
								'name' => 'Activity:',
								'id' => 'activity',
								'type' => 'text'
							),
						),
					),

				),
			),
		),
	);

	$meta_boxes[] = array(
		'title'      => 'Información',
		'post_types' => array('online'),
		'fields'     => array(

			array(
				'group_title' => 'Class:',
				'id'     => 'class_group',
				'type'   => 'group',
				'clone'  => true,
				'sort_clone' => true,
				'collapsible' => true,
				'fields' => array(
					array(
						'id'               => 'class_image',
						'name'             => 'Image class',
						'type'             => 'image_advanced',

						// Delete image from Media Library when remove it from post meta?
						// Note: it might affect other posts if you use same image for multiple posts
						'force_delete'     => false,

						// Maximum image uploads.
						'max_file_uploads' => 1,

						// Do not show how many images uploaded/remaining.
						'max_status'       => 'false',

						// Image size that displays in the edit page. Possible sizes small,medium,large,original
						'image_size'       => 'thumbnail',
					),
					array(
						'name' => 'Class name:',
						'id' => 'class_name',
						'type'       => 'taxonomy_advanced',
						'taxonomy'   => 'class',
						'field_type' => 'select',
					),

					array(
						'group_title' => 'Activities:',
						'id'     => 'calendar_activities',
						'type'   => 'group',
						'clone'  => true,
						'sort_clone' => true,
						'collapsible' => true,
						'fields' => array(
							array(
								'type'       => 'taxonomy_advanced',
								'name'       => 'Time',
								'id'         => 'time_selection',
								'taxonomy'   => 'time',
								'field_type' => 'select',
							),
							array(
								'name' => 'Day activity:',
								'id' => 'day_activity',
								'type' => 'text'
							),
							array(
								'group_title' => 'Hour:',
								'id' => 'hour_activity_group',
								'type'   => 'group',
								'clone'  => true,
								'sort_clone' => true,
								'collapsible' => true,
								'fields' => array(
									array(
										'name' => 'Hour',
										'id' => 'hour_activity',
										'type' => 'text'
									),
								),
							),
						),
					),


				),
			),
		),
	);

	$meta_boxes[] = array(
		'title' => __('Information', 'textdomain'),
		'post_types' => array('Our Teachers'),
		'fields' => array(
			array(
				'name'		  => 'Type:',
				'id'          => 'teacher_type',
				'type'        => 'text',
				'type'       => 'taxonomy_advanced',
				'taxonomy'   => 'types',
				'field_type' => 'select',
			),
			array(
				'name'		  => 'Position:',
				'id'          => 'teacher_position',
				'type'       => 'taxonomy_advanced',
				'taxonomy'   => 'position',
				'field_type' => 'select',
			),
			array(
				'name'		  => 'Country:',
				'id'          => 'teacher_contry',
				'type'       => 'taxonomy_advanced',
				'taxonomy'   => 'country',
				'field_type' => 'select',
			),
			array(
				'name'		  => 'Priority:',
				'id'          => 'teacher_priority',
				'type' => 'checkbox',
				'std'  => 1, // 0 or 1
			),
		),
	);

	return $meta_boxes;
}
