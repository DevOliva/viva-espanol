<?php /* Template Name: Online */

get_header();


$page = array(
    'page_id' => 116,
    'post_type' => 'page',
    'post_status' => 'publish'
);

$my_query = new WP_Query($page);

while ($my_query->have_posts()) : $my_query->the_post();

?>

    <section class="main-section-nopadding online-ves" id="online">
        <div class="container-fluid">
            <div class="row">
                <div class="online-ves__intro">
                    <h1 class="color-white">Classes</h1>
                    <p class="color-white"><?php echo get_the_content(); ?></p>
                </div>
                <div class="online-ves__types">
                    <div class="container-max-dk">
                        <div class="online-ves__types--wrapper">
                        <?php 

										# Parent ID
										$parent_id = 116;

										# Arguments for the query
										$args = array(
											'post_parent' => $parent_id,
											'post_type'   => 'page', 
											'posts_per_page' => -1,
											'post_status' => 'publish' 
											); 

										# The parent's children
										$resources = get_children( $args );

										# Current pages ID
										$page_ID = get_the_ID();

										# Start iterating from 1, to skip first child
										foreach( $resources as $resource ) {
											$id = $resource->ID;
											$url = get_permalink( $id );
                                            $title = $resource->post_title;
                                            $image = get_the_post_thumbnail_url( $id );
	
									?>
                            <div class="online-ves__types--item">
                                <div class="online-ves__types--image <?php echo str_replace(' ', '-', strtolower($title)); ?> bg-yellow">
                                    <a href="
                                    <?php if($title == 'Classes'){
                                        echo get_home_url().'/online/';
                                    }else{
                                        echo $url;
                                    }
                                    ?>">
                                        <img src="<?= $image; ?>" alt="">
                                    </a>
                                </div>
                                <div class="online-ves__types--text">
                                <a href="
                                    <?php if($title == 'Classes'){
                                        echo get_home_url().'/online/';
                                    }else{
                                        echo $url;
                                    }
                                    ?>">
                                        <h3 class="text-center color-rose"><?= $title ?></h3>
                                    </a>
                                </div>
                            </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--div class="online-ves__cta">
                    <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
                </div-->
            </div>
        </div>
    </section>


    <section class="main-section-nopadding online-table-ves" id="online-table">
        <div class="online-table-ves__search">
            <select name="" id="">
                <option value="" disabled selected>Select a time zone</option>
                <?php 

                        $query = new WP_Query(array(
                            'post_type' => 'online',
                            'posts_per_page' => 1
                        ));


                        while ($query->have_posts()) : $query->the_post();

                        $group_days = get_post_meta($post->ID, 'class_group', true);

                        for ($j = 0; $j < count($group_days[0]['calendar_activities']); $j++) {
       
                            $time = get_term($group_days[0]['calendar_activities'][$j]['time_selection']);

                            //echo $time->name;
                            echo '<option  value="'. strtolower($time->name) .'">'.$time->name.'</option>';
                        }

                    endwhile;
                    ?>
            </select>
        </div>
        <table class="online-table-ves__desktop">
            <thead>
                <tr>
                    <th background="<?php echo get_template_directory_uri() ?>/src/dist/assets/images/bg-gallery.jpg" style="width: 300px;"></th>
                    <th class="tg-tyg4">CLASS</th>
                    <?php 

                        $query = new WP_Query(array(
                            'post_type' => 'online',
                            'posts_per_page' => 1
                        ));


                        while ($query->have_posts()) : $query->the_post();

                        $group_days = get_post_meta($post->ID, 'class_group', true);

                        for ($j = 0; $j < count($group_days[0]['calendar_activities']); $j++) {
       
                            $time = get_term($group_days[0]['calendar_activities'][$j]['time_selection']);

                            //echo $time->name;
                            echo '<th class="tg-tyg4 '. strtolower($time->name) .'">Days/Times<br>('.$time->name.')</th>';
                        }


                    ?>
                    <!-- <th class="tg-tyg4">Days/Times<br>(PDT)</th>
                    <th class="tg-tyg4">Days/Times<br>(PDT)</th>
                    <th class="tg-tyg4">Days/Times<br>(PDT)</th>
                    <th class="tg-tyg4">Days/Times<br>(PDT)</th> -->

                    <?php endwhile; ?>
                </tr>
            </thead>
            <tbody>

            <?php

                while ($query->have_posts()) : $query->the_post();
            
            // foreach($group_days as $group_day){
                for($l = 0; $l < count($group_days); $l++){
                $class = get_term($group_days[$l]['class_name']);
                // if($l > 0){
                //     $classPrev = get_term($group_days[$l -1]['class_name']);
                // }
                

                $imageClasses = $group_days[$l]['class_image'];
                foreach ( $imageClasses as $imageClass ) {
                    $imageClass = RWMB_Image_Field::file_info( $imageClass, array( 'size' => 'full' ) );
                }

                $color = explode('-',trim($class->slug));
                $colorPrev = explode('-',trim($classPrev->slug));
                echo '<tr class="table-color-'.$color[0].'">';

                echo '<td rowspan="'.$count.'" style="width: 300px;"><img src="'.$imageClass['url'].'" /></td>';

                
                
                echo '<td><b>'.$class->name.'</b></td>';
                
                for($i = 0; $i < count($group_days[$l]['calendar_activities']); $i++){

                    $time = get_term($group_days[0]['calendar_activities'][$i]['time_selection']);

                    echo '<td class="'. strtolower($time->name) .'">';
                    $day = $group_days[$l]['calendar_activities'][$i]['day_activity'];
                    $activity = $group_days[$l]['calendar_activities'][$i]['hour_activity'];
                    echo '<b>'.$day.'</b><br/>';
                    //echo var_dump($group_day['calendar_activities'][$i]['hour_activity_group']);
                    for($k = 0; $k < count($group_days[$l]['calendar_activities'][$i]['hour_activity_group']); $k++){
                        echo $group_days[$l]['calendar_activities'][$i]['hour_activity_group'][$k]['hour_activity'].'<br/>';
                    }
                    echo '</td>';

                    // for($j = 0; $j < count($group_day['calendar_activities'][$i]['hour_activity_group']); $j++){
                    //     echo $group_day['calendar_activities'][$i]['hour_activity'][$j].'<br/>';
                    // }
                }
                
                echo '</tr>';
            }

        endwhile;


                                // for ($j = 0; $j < count($group_days[0]['calendar_activities']); $j++) {

                                //     // Entra 5 veces
                                //     echo '<tr>';
           
                                //         for ($i = 0; $i < count($group_days); $i++) {
                                //         // Entra 4 veces
                                //         $hour = get_term($group_days[$i]['calendar_activities'][$j]['hour_selection']);
                                        
                                //         if ($i == 0) {
                                            
                                //             echo '<td>' . $hour->name . '</td>';
                                //         }
                                //         echo '<td>' . $group_days[$i]['calendar_activities'][$j]['activity'] . '</td>';
                                //     }
                                //     echo '</tr>';
                                // }


                                ?>

                <!--tr class="table-color-yellow">
                    <td style="background-color: #ffb510;"><img src="https://via.placeholder.com/300x150?text=Image" alt="Image" width="300" height="112"></td>
                    <td class="table-color-yellow"><b>Pre /Kindergarde<br>(age 4-5)</b></td>
                    <td class="table-color-yellow"><b>Monday</b><br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td class="table-color-yellow"><b>Monday</b><br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td class="table-color-yellow"><b>Monday</b><br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td class="table-color-yellow"><b>Monday</b><br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                </tr>
                <tr class="table-color-blue">
                    <td rowspan="3" style="background-color: #5ba1c5"><img src="https://via.placeholder.com/300x150?text=Image" alt="Image"></td>
                    <td>Pre /Kindergarde<br>(age 4-5)</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                </tr>
                <tr class="table-color-blue">
                    <td>Pre /Kindergarde<br>(age 4-5)</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                </tr>
                <tr class="table-color-blue">
                    <td>Pre /Kindergarde<br>(age 4-5)</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                    <td>Monday<br>8.30 - 9<br>8.30 - 9<br>8.30 - 9</td>
                </tr-->
            </tbody>
        </table>

        <div class="online-table-ves__mobile">
            <div class="online-table-ves__header">
                <p class="text-center color-purple">Class</p>
            </div>
            <div class="online-table-ves__body">
                <ul>
                    <?php 

                        while ($query->have_posts()) : $query->the_post();

                        $group_days = get_post_meta($post->ID, 'class_group', true);
                                 
                        for($l = 0; $l < count($group_days); $l++){
                       
                            $class = get_term($group_days[$l]['class_name']);
                            $color = explode('-',trim($class->slug));

                            echo '<li class="table-color-'.$color[0].'">';
                            echo '<a href=""><b>'.$class->name.'</b></a>'; ?>
                        <div class="online-table-ves__content">
                            <ul>
                                <?php
                                
                 
            
                                // foreach($group_days as $group_day){
                                    for ($j = 0; $j < count($group_days[0]['calendar_activities']); $j++) {
       
                                        $time = get_term($group_days[$j]['calendar_activities'][$j]['time_selection']);
                                    
                                        echo '<li>';
                                        echo '<p>Day/Times<br/> '.$time->name.'</p>';
                                        echo '<ul>';
                                        $day = $group_days[$j]['calendar_activities'][$j]['day_activity'];
                                        echo '<li><b>'.$day.'</b></li>';

                                        for($k = 0; $k < count($group_days[$l]['calendar_activities'][$j]['hour_activity_group']); $k++){
                                            echo '<li>'.$group_days[$l]['calendar_activities'][$j]['hour_activity_group'][$k]['hour_activity'].'</li>';
                                        }

                                        
                                        echo '</ul>';
                                        echo '</li>';
                                             
                                }            
                                    
                                ?>
                            </ul>
                        </div>
                        
                        <?php
                            echo '</li>';
                        }

                    endwhile;
                        
                    
                    ?>
                   

                </ul>
            </div>


        </div>


    </section>

    <section class="main-section-nopadding table-ves-cta" id="online-table">
        <div class="container-fluid">
            <div class="row">
                <div class="table-ves-cta__content">
                    <a class="btn bordered centered background-purple background-hover-purple-line color-white large" href="https://campscui.active.com/orgs/LamorindaSpanishIncdbaVivaelEspanol#/selectSessions/2873804" target="_blank">Register now</a>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>

<?php 
$page = array(
    'page_id' => 304,
    'post_type' => 'page',
    'post_status' => 'publish'
);

$my_query = new WP_Query($page);

while ($my_query->have_posts()) : $my_query->the_post();

$thecontent = get_the_content();
if(!empty($thecontent)) {
?>

<section class="main-section camps-ves">
        <div class="container-fluid">
            <div class="row">
                <div class="container-max-dk">
                    <div class="camps-ves__container">
                        <?php 
                            the_content();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } endwhile; ?>

    <!--section class="main-section-nopadding online-ves" id="online">
        <div class="container-fluid">
            <div class="row">
                <div class="online-ves__types">
                <div class="online-ves__cta">
                    <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
                </div>
                </div>
                
            </div>
        </div>
    </section-->


<?php
get_footer();
