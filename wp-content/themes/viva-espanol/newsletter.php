<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require( '../../../wp-load.php' );
global $wpdb;

$email              = isset($_POST['email_newsletter']) ? $_POST['email_newsletter'] : null;
$user_registered	= date('Y-m-d H:i:s');


if ($email != null){

    $query   = $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}newsletter WHERE email = %s", $email );
    $results = $wpdb->get_results( $query );


    if ( count( $results ) > 0 ) {
        // Email already exits.
        echo 'The User is already registered';
    }else{
        $query = $wpdb->query( $wpdb->prepare(
            "
            INSERT INTO wp_newsletter
            ( email, date)
            VALUES (%s, %s)
            ",
            $email,
            $user_registered
            ));

            if ($query) {
                echo '¡Congrats! You registered to our newsletter';
            }
    }

}
