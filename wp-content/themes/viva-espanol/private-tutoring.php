<?php /* Template Name: Private Tutoring */

get_header();

global $post;

$parent = get_post( $post->post_parent );

$page = array(
'page_id' => $post->ID,
'post_type' => 'page',
'post_status' => 'publish' );

$my_query = new WP_Query($page);


while($my_query->have_posts()) : $my_query->the_post();
    if($parent->post_title == 'Online'){
?>

<section class="main-section-nopadding online-ves" id="online">
        <div class="container-fluid">
            <div class="row">
                <div class="online-ves__intro">
                    <h1 class="color-white"><?php the_title(); ?></h1>
                    <p class="color-white"><?php echo get_the_excerpt(); ?></p>
                </div>
                <div class="online-ves__types">
                    <div class="container-max-dk">
                        <div class="online-ves__types--wrapper">
                        <?php 

										# Parent ID
										$parent_id = 116;

										# Arguments for the query
										$args = array(
											'post_parent' => $parent_id,
											'post_type'   => 'page', 
											'posts_per_page' => -1,
											'post_status' => 'publish' 
											); 

										# The parent's children
										$resources = get_children( $args );

										# Current pages ID
										$page_ID = get_the_ID();

										# Start iterating from 1, to skip first child
										foreach( $resources as $resource ) {
											$id = $resource->ID;
											$url = get_permalink( $id );
                                            $title = $resource->post_title;
                                            $image = get_the_post_thumbnail_url( $id );
	
									?>
                            <div class="online-ves__types--item">
                                <div class="online-ves__types--image <?php echo str_replace(' ', '-', strtolower($title)); ?> bg-yellow">
                                    <a href="
                                    <?php if($title == 'Classes'){
                                        echo get_home_url().'/online/';
                                    }else{
                                        echo $url;
                                    }
                                    ?>">
                                        <img src="<?= $image; ?>" alt="">
                                    </a>
                                </div>
                                <div class="online-ves__types--text">
                                <a href="
                                    <?php if($title == 'Classes'){
                                        echo get_home_url().'/online/';
                                    }else{
                                        echo $url;
                                    }
                                    ?>">
                                        <h3 class="text-center color-rose"><?= $title ?></h3>
                                    </a>
                                </div>
                            </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--div class="online-ves__cta">
                    <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
                </div-->
            </div>
        </div>
    </section>

    <?php }else{?>

<section class="main-section-nopadding person-ves" id="in-person">
    <div class="container-fluid">
        <div class="row">
            <div class="person-ves__intro">
                <h1 class="color-white">In-Person <?php the_title(); ?></h1>
                <p class="color-white"><?php echo get_the_excerpt(); ?></p>
            </div>
            <!--div class="person-ves__cta">
                <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
            </div-->
            <div class="person-ves__content">
                <div class="person-ves__selection">                    
                    <?php 
                        if($parent->post_title == 'In person'){
                            //echo '<p class="text-center color-purple"><strong>Please select your location</strong></p>';
                            include_once('template-parts/in-person-selection.php'); 
                        }else{
                            include_once('template-parts/online-selection.php'); 
                        }
                        
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php } endwhile; ?>

<?php 
while($my_query->have_posts()) : $my_query->the_post();
?>

<section class="main-section camps-ves">
        <div class="container-fluid">
            <div class="row">
                <div class="container-max-dk">
                    <div class="camps-ves__container">
                        <?php 
                            the_content();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endwhile; ?>

    <?php 
     if($parent->post_title == 'Online'){?>

    <!--section class="main-section-nopadding online-ves" id="online">
        <div class="container-fluid">
            <div class="row">
                <div class="online-ves__types">
                <div class="online-ves__cta">
                    <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
                </div>
                </div>    
            </div>
        </div>
    </section-->

    <?php 
     }?>


<?php
    get_footer();

