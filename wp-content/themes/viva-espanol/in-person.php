<?php /* Template Name: In Person */

get_header();

?>

<section class="main-section-nopadding person-ves" id="in-person">
    <div class="container-fluid">
        <div class="row">
            <div class="person-ves__content">
                <div class="person-ves__selection">
                    <ul>
                        <li>
                        <div class="custom-select">
                            <select>
                                <option value="0">Classes</option>
                                <option value="0">Lafayette</option>
                                <option value="1">San Rafael</option>
                                <option value="2">Pleasanton</option>
                                <option value="3">At School</option>
                            </select>
                            </div>
                        </li>
                        <li>
                        <div class="custom-select">
                            <select>
                                <option value="">Camps</option>
                                <option value="0">Lafayette</option>
                                <option value="1">San Rafael</option>
                                <option value="2">Pleasanton</option>
                            </select>
                            </div>
                        </li>
                        <li>
                            <a href="">Events</a>
                        </li>
                        <li>
                            <a href="">Private tutoring</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="person-ves__intro">
                <p class="color-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur quia nisi expedita ad impedit veniam. Earum quaerat deleniti facere, nesciunt quidem placeat exercitationem voluptas ipsa possimus accusantium! In, quae ad.</p>
            </div>
            <!--div class="person-ves__cta">
                <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
            </div-->
        </div>
    </div>
</section>

<section class="main-section person-ves__table" id="person-table">
    <div class="container-fluid">
        <div class="row">
            <div class="container-max-dk">
                <table class="person-ves__table--lafayette person-ves__table__desktop">
                    <thead>
                        <tr>
                            <th colspan="7">LAFAYETTE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="months">
                            <td>HOURS</td>
                            <td>MON</td>
                            <td>TUE</td>
                            <td>WED</td>
                            <td>THU</td>
                            <td>FRI</td>
                            <td>SAT</td>
                        </tr>
                        <tr>
                            <td>9:30-10:15</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Parent & Me "Música</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>9:30-10:15</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Pre-School / Kindergarden</td>
                            <td></td>
                            <td>Pre-School / Kindergarden</td>
                        </tr>
                    </tbody>
                </table>
                <div class="person-ves__table__mobile person-ves__table--lafayetter">
                    <div class="person-ves__table__mobile--header">
                        <p class="text-center">LAFAYETTE</p>
                    </div>
                    <div class="person-ves__table__mobile--body">
                        <ul>
                            <li>
                                <a href="">Monday</a>
                                <div class="person-ves__table__mobile--content">
                                    <ul>
                                        <li>
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th>Hours</th>
                                                        <th>Activity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>9:30-10:15</td>
                                                        <td>Parent & Me "Música</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="main-section-nopadding table-ves-cta" id="online-table">
    <div class="container-fluid">
        <div class="row">
            <div class="table-ves-cta__content">
                <a class="btn bordered centered background-purple background-hover-purple-line color-white large" href="https://campscui.active.com/orgs/LamorindaSpanishIncdbaVivaelEspanol#/selectSessions/2873804" target="_blank">Register now</a>
            </div> 
        </div>
    </div>
</section>

<?php
get_footer();