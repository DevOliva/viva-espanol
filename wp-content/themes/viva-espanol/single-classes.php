<?php

get_header();

global $post;

rewind_posts();

$page = array(
    'page_id' => 76,
    'post_type' => 'page',
    'post_status' => 'publish'
);

$my_query = new WP_Query($page);
$title_section = get_the_title();

while ($my_query->have_posts()) : $my_query->the_post();

?>

<section class="main-section-nopadding person-ves" id="in-person">
    <div class="container-fluid">
        <div class="row">
            <div class="person-ves__intro">
                <h1 class="color-white">In-Person <?= $title_section; ?></h1>
                <p class="color-white"><?= get_the_content(); ?></p>
            </div>
            <!--div class="person-ves__cta">
                <a href="<?php echo get_home_url(); ?>/about-us" class="btn bordered centered bg-green background-hover-green--line">More information about our programs</a>
            </div-->
            <div class="person-ves__content">
                <div class="person-ves__selection">
                    <!--p class="text-center color-purple"><strong>Please select your location</strong></p-->
                    <?php include_once('template-parts/in-person-selection.php'); ?>
                </div>
            </div>
        </div>
    </div>
</section>


    <?php

endwhile;
if (have_posts()) : while (have_posts()) : the_post();

$block = get_post_meta($post->ID, 'block_page', true);

    ?>

        <section class="main-section person-ves__table" id="person-table">
            <div class="container-fluid">
                <div class="row">
                    <div class="container-max-dk">
                        <table class="person-ves__table--lafayette person-ves__table__desktop" <?php if($block){ echo 'style="display:none;"';} ?>>
                            <thead>
                                <tr>
                                    <?php $group_days = get_post_meta($post->ID, 'calendar_group', true);  $count = count($group_days); ?>
                                    <th colspan="<?= $count + 1; ?>"><?php echo strtoupper(get_the_title()); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="months">
                                    <td>HOURS</td>
                                    <?php
                                    $group_days = get_post_meta($post->ID, 'calendar_group', true);
                                    foreach ($group_days as $group_day) {
                                        $day = get_term($group_day['day_selection']);
                                    ?>

                                        <td><?php echo strtoupper($day->slug); ?></td>

                                    <?php } ?>

                                </tr>

                                <?php

                                $group_days = get_post_meta($post->ID, 'calendar_group', true);

                                for ($j = 0; $j < count($group_days[0]['calendar_activities']); $j++) {

                                    // Entra 5 veces
                                    echo '<tr>';
           
                                        for ($i = 0; $i < count($group_days); $i++) {
                                        // Entra 4 veces
                                        $hour = get_term($group_days[$i]['calendar_activities'][$j]['hour_selection']);
                                        
                                        if ($i == 0) {
                                            
                                            echo '<td>' . $hour->name . '</td>';
                                        }
                                        echo '<td>' . $group_days[$i]['calendar_activities'][$j]['activity'] . '</td>';
                                    }
                                    echo '</tr>';
                                }

                                ?>

                            </tbody>
                        </table>

                        <div class="person-ves__table__mobile person-ves__table--lafayetter" <?php if($block){ echo 'style="display:none;"';} ?>>
                            <div class="person-ves__table__mobile--header">
                                <p class="text-center"><?php echo strtoupper(get_the_title()); ?></p>
                            </div>
                            <div class="person-ves__table__mobile--body">
                                <ul>
                                    <?php

                                    $group_days = get_post_meta($post->ID, 'calendar_group', true);

                                    for ($k = 0; $k < count($group_days); $k++) {
                                        $day = get_term($group_days[$k]['day_selection']);

                                    ?>
                                        <li>
                                            <a href=""><?= $day->name; ?></a>
                                            <div class="person-ves__table__mobile--content">
                                                <table>
                                                    <thead>
                                                        <tr>

                                                            <th>Hours</th>
                                                            <th>Activity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php

                                                        for ($l = 0; $l < count($group_days[$k]['calendar_activities']); $l++) {
                                                            if (strlen($group_days[$k]['calendar_activities'][$l]['activity']) > 2) {
                                                                echo '<tr>';
                                                                $hour = get_term($group_days[$k]['calendar_activities'][$l]['hour_selection']);
                                                                echo '<td>' . $hour->name . '</td>';
                                                                echo '<td>' . $group_days[$k]['calendar_activities'][$l]['activity'] . '</td>';
                                                                echo '</tr>';
                                                            }
                                                        }

                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $thecontent = get_the_content();
        if(!empty($thecontent)) { ?>
        <section class="main-section person-ves__table" id="person-table-text">
            <div class="container-fluid">
                <div class="row">
                    <div class="container-max-dk">
                        <div class="person-ves__disclaimer">
                            <?php the_content(); ?>
                        </div>               
                    </div>
                </div>
            </div>
         </section>
         <?php } ?>


<?php if($title_section == 'At Your School'){ ?>

    <section class="main-section store-ves">
        <div class="container-fluid">
            <div class="row">
                    <div class="camps-ves__container">
                        <h2 class="text-center">School Locator</h2>
                        <?php echo do_shortcode('[wpsl template="default" map_type="roadmap" auto_locate="true" star_location="california"]'); ?>
                    </div>
            </div>
        </div>
    </section>

<?php } ?>

<?php endwhile;
endif; ?>

<?php
get_footer();
