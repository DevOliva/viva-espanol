<?php
/**
 * Enqueue theme assets
 *
 * @package Aquila
 */

namespace VES_THEME\Inc;

use VES_THEME\Inc\Traits\Singleton;

class Assets {
	use Singleton;

	protected function __construct() {

		// load class.
		$this->setup_hooks();
	}

	protected function setup_hooks() {

		/**
		 * Actions.
		 */
		add_action( 'wp_enqueue_scripts', [ $this, 'register_styles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_scripts' ] );
		/**
		 * The 'enqueue_block_assets' hook includes styles and scripts both in editor and frontend,
		 * except when is_admin() is used to include them conditionally
		 */
		//add_action( 'enqueue_block_assets', [ $this, 'enqueue_editor_assets' ] );
	}

	public function register_styles() {
		// Register styles.

    wp_register_style( 'core-css', VES_DIST_CSS_URI . '/core.min.css', [], false, 'all' );
    wp_register_style( 'main-css', VES_DIST_CSS_URI . '/style.min.css', [], false, 'all' );

		// wp_register_style( 'bootstrap-css', VES_DIST_LIB_URI . '/css/bootstrap.min.css', [], false, 'all' );
		// wp_register_style( 'slick-css', VES_DIST_LIB_URI . '/css/slick.css', [], false, 'all' );
		// wp_register_style( 'slick-theme-css', VES_DIST_LIB_URI . '/css/slick-theme.css', ['slick-css'], false, 'all' );
		//wp_register_style( 'main-css', VES_DIST_CSS_URI . '/style.min.css', [], filemtime( VES_DIST_CSS_DIR_PATH . '/style.min.css' ), 'all' );

		// Enqueue Styles.
		wp_enqueue_style( 'core-css' );
		// wp_enqueue_style( 'slick-css' );
		// wp_enqueue_style( 'slick-theme-css' );
		wp_enqueue_style( 'main-css' );
	}

	public function register_scripts() {
		// Register scripts.
		wp_register_script( 'core-js', VES_DIST_JS_URI . '/main-core.min.js', [], false, true );
    wp_register_script( 'main-js', VES_DIST_JS_URI . '/main.min.js', [], false, true );
		//wp_register_script( 'main-js', VES_DIST_JS_URI . '/main.min.js', [], filemtime( VES_DIST_JS_DIR_PATH . '/main.min.js' ), true );
		//wp_register_script( 'bootstrap-js', VES_DIST_LIB_URI . '/js/bootstrap.min.js', ['jquery'], false, true );

		// Enqueue Scripts.
		wp_enqueue_script( 'core-js' );
		//wp_enqueue_script( 'bootstrap-js' );
		wp_enqueue_script( 'main-js' );
	}

	/**
	 * Enqueue editor scripts and styles.
	 */
	public function enqueue_editor_assets() {

		$asset_config_file = sprintf( '%s/assets.php', VES_DIST_PATH );

		if ( ! file_exists( $asset_config_file ) ) {
			return;
		}

		$asset_config = require_once $asset_config_file;

		if ( empty( $asset_config['js/editor.js'] ) ) {
			return;
		}

		$editor_asset    = $asset_config['js/editor.js'];
		$js_dependencies = ( ! empty( $editor_asset['dependencies'] ) ) ? $editor_asset['dependencies'] : [];
		$version         = ( ! empty( $editor_asset['version'] ) ) ? $editor_asset['version'] : filemtime( $asset_config_file );

		// Theme Gutenberg blocks JS.
		if ( is_admin() ) {
			wp_enqueue_script(
				'aquila-blocks-js',
				AQUILA_DIST_JS_URI . '/blocks.js',
				$js_dependencies,
				$version,
				true
			);
		}

		// Theme Gutenberg blocks CSS.
		$css_dependencies = [
			'wp-block-library-theme',
			'wp-block-library',
		];

		wp_enqueue_style(
			'aquila-blocks-css',
			VES_DIST_CSS_URI . '/blocks.css',
			$css_dependencies,
			filemtime( VES_DIST_CSS_DIR_PATH . '/blocks.css' ),
			'all'
		);

	}


}
