<?php

/**
 * Clock Widget
 *
 * @package Aquila
 */

namespace VES_THEME\Inc;

use WP_Widget;

use VES_THEME\Inc\Traits\Singleton;

class Newsletter extends WP_Widget
{

	use Singleton;

	/**
	 * Register widget with WordPress.
	 */
	public function __construct()
	{
		parent::__construct(
			'newsletter', // Base ID
			'Newsletter', // Name
			['description' => __('Newsletter', 'ves'),] // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 *
	 * @see WP_Widget::widget()
	 *
	 */
	public function widget($args, $instance)
	{
		extract($args);

		$title = apply_filters('widget_title', $instance['title']);

		echo $before_widget;

		if (!empty($title)) {
			echo $before_title . $title . $after_title;
		}
?>
		<div class="footer-newsletter">
			<a class="btn bg-green bordered medium" href="https://visitor.r20.constantcontact.com/manage/optin?v=001sI1gWfQ1ijn6-oyxGSLIU8UtSoomPvgz9HJmjNi59GEDC_7X3YIiHVc3_YsmHIEroCbgFm76nfTwLXlzaWbk9qqSEff9dSg5kL7p_TcIIz8%3D" target="_blank">Subscribe</a>
			<!--form>
				<input type="text" name="email_newsletter" value="">
				<p class="form-message"></p>
				<button class="btn bg-green bordered medium" type="submit">Send</button>
			</form-->	
		</div>
	<?php
		echo $after_widget;
	}

	/**
	 * Back-end widget form.
	 *
	 * @param array $instance Previously saved values from database.
	 *
	 * @see WP_Widget::form()
	 *
	 */
	public function form($instance)
	{
		if (isset($instance['title'])) {
			$title = $instance['title'];
		} else {
			$title = __('New title', 'ves');
		}
	?>
		<p>
			<label for="<?php echo $this->get_field_name('title'); ?>"><?php _e('Title:', 'ves'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
		</p>
<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 * @see WP_Widget::update()
	 *
	 */
	public function update($new_instance, $old_instance)
	{
		$instance          = [];
		$instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

		return $instance;
	}
}
