<?php
/**
 * Theme Sidebars.
 *
 * @package Aquila
 */

namespace VES_THEME\Inc;

use VES_THEME\Inc\Traits\Singleton;

/**
 * Class Assets
 */
class Sidebars {

	use Singleton;

	/**
	 * Construct method.
	 */
	protected function __construct() {
		$this->setup_hooks();
	}

	/**
	 * To register action/filter.
	 *
	 * @return void
	 */
	protected function setup_hooks() {

		/**
		 * Actions
		 */
		add_action( 'widgets_init', [ $this, 'register_sidebars' ] );
		add_action( 'widgets_init', [ $this, 'register_newsletter' ] );

	}

	/**
	 * Register widgets.
	 *
	 * @action widgets_init
	 */
	public function register_sidebars() {

		register_sidebar( array(
			'name'          => 'Footer area one',
			'id'            => 'footer_area_one',
			'description'   => 'This widget area discription',
			'before_widget' => '<section class="footer-area">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="footer-title text-left">',
			'after_title'   => '</h3>',
		  ));
		
		  register_sidebar( array(
			'name'          => 'Footer area two',
			'id'            => 'footer_area_two',
			'description'   => 'This widget area discription',
			'before_widget' => '<section class="footer-area">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="footer-title text-right">',
			'after_title'   => '</h3>',
		  ));
		
		  register_sidebar( array(
			'name'          => 'Footer area three',
			'id'            => 'footer_area_three',
			'description'   => 'This widget area discription',
			'before_widget' => '<section class="footer-area">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		  ));
		  

		  register_sidebar( array(
			'name'          => 'Footer area four',
			'id'            => 'footer_area_four',
			'description'   => 'This widget area discription',
			'before_widget' => '<section class="footer-area">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		  ));
	  
	}

	public function register_newsletter() {
		register_widget( 'VES_THEME\Inc\Newsletter' );
	}

}
  
  
